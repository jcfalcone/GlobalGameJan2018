﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointGizmos : MonoBehaviour 
{
	void OnDrawGizmos() 
	{
        Gizmos.DrawIcon(transform.position, "Spawn.png", true);
    }
}
