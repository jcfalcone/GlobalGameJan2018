﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchDoorDown : Interactable {

    /** Object to move */
    [SerializeField]
    private GameObject InteractiveObject;

    /** Speed that it moves at */
    [SerializeField]
    private float speed = 1.5f;

    /** Move to target */
    [SerializeField]
    private Transform target;

    /** track speed to move at */
    private float step;

    /** Override function */
    public override void Interact()
    {
        base.Interact();
        if (InteractiveObject.name == "Door")
            SoundManager.instance.Play(SoundManager.AudioClips.DoorOpening);
    }

    /** Update step */
    public override void Update()
    {
        base.Update();
        step = speed * Time.deltaTime;

        if (hasInteracted)
            HasInteracted();
    }

    /** Move Downward */
    void HasInteracted()
    {
        InteractiveObject.transform.position = Vector3.MoveTowards(InteractiveObject.transform.position, target.position, step);
    }
}