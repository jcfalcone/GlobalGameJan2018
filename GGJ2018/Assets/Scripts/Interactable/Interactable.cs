﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour {

    /** Radius of interaction */
    [SerializeField]
    private float interactRadius;

    /** Check if object can be interacted with more than once */
    [SerializeField]
    private bool multipleInteraction;

    /** Check if object has already been interacted with */
    protected bool hasInteracted;

    /** Store reference to player */
    private GameObject player;

    /** Store reference to the position of the object */
    private Transform thisObject;

    /** Can be overriden by child object */
    public virtual void Interact()
    {
        Debug.Log("Interact with " + transform.name);
    }

    /** Assign player GameObject and thisObject transform reference */
    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        thisObject = GetComponent<Transform>();
    }

    /** Checking if the player  */
    public virtual void Update()
    {
        /** Check if the object has already been interacted */
        if (!hasInteracted)
        {   
            /** Confirm player is within radius and pressed "E" */
            float distance = Vector3.Distance(player.transform.position, thisObject.position);
            if (distance <= interactRadius && Input.GetKeyDown(KeyCode.E))
            {
                /** Override via child script */
                Interact();
                
                /** Set the object to no longer be interactable if One-Time interaction */
                if (!multipleInteraction)
                    hasInteracted = true;
            }
        }
    }

    /** Used to see the radius of the object in Unity Editor */
    private void OnDrawGizmosSelected()
    {
        if (thisObject == null)
            thisObject = this.transform;

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(thisObject.position, interactRadius);
    }
}
