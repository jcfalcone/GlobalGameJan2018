﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovablePlatform : MonoBehaviour {
    
    /** Object to move */
    [SerializeField]
    private GameObject ObjectGFX;

    /** Speed that it moves at */
    [SerializeField]
    private float speed = 2.0f;

    /** Move to target */
    [SerializeField]
    private Transform target;

    /** track speed to move at */
    private float step;

    /** Check position status */
    [SerializeField]
    private Transform origin;

    /** Handles which direction to move */
    [SerializeField]
    private bool toOrigin = false;

    /** Update step */
    private void Update()
    {
        step = speed * Time.deltaTime;

        if (toOrigin)
        {
            MoveToOrigin();
        }

        else if (!toOrigin)
        {
            MoveToTarget();
        }
    }

    /** Move Downward */
    void MoveToTarget()
    {
        ObjectGFX.transform.position = Vector3.MoveTowards(ObjectGFX.transform.position, target.position, step);
        if(ObjectGFX.transform.position == target.position)
        {
            toOrigin = true;
        }
    }

    /** Move Downward */
    void MoveToOrigin()
    {
        ObjectGFX.transform.position = Vector3.MoveTowards(ObjectGFX.transform.position, origin.position, step);
        if (ObjectGFX.transform.position == origin.position)
        {
            toOrigin = false;
        }
    }
}
