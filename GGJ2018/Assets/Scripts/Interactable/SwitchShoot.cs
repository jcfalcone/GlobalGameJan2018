﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchShoot : MonoBehaviour {

    /** Object to move */
    [SerializeField]
    private GameObject InteractiveObject;

    /** Speed that it moves at */
    [SerializeField]
    private float speed = 2.0f;

    /** Move to target */
    [SerializeField]
    private Transform target;

    /** track speed to move at */
    private float step;

    /** Check if switch was shot */
    private bool _triggered;

    /** Update step */
    private void Update()
    {
        step = speed * Time.deltaTime;

        if (_triggered)
            Triggered();
    }

    /** Set when shot by player controller */
    public void WasTriggered(bool triggered)
    {
        _triggered = triggered;
        
        if (InteractiveObject.name == "Door")
            SoundManager.instance.Play(SoundManager.AudioClips.DoorOpening);
        if (InteractiveObject.name == "Platform")
            SoundManager.instance.Play(SoundManager.AudioClips.ElevatorMoving);
    }

    /** Called when _triggered = true */
    private void Triggered()
    {
        InteractiveObject.transform.position = Vector3.MoveTowards(InteractiveObject.transform.position, target.position, step);
    }
}