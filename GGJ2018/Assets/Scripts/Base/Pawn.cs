﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Pawn : Actor 
{
    [SerializeField]
    [SyncVar]
    protected int m_Health;

    public int health
    {
        get
        {
            return this.m_Health;
        }
        set
        {
            this.m_Health = value;
        }
    }

    public bool isAlive
    {
        get
        {
            if(this.m_Health > 0)
            {
                return true;
            }

            return false;
        }
    }

    public int Damage;

    public virtual void ApplyDamage(int _damage)
    {
        this.health -= _damage;

        if(!this.isAlive)
        {
            this.Die();
        }
    }

    public virtual void Die()
    {
        Destroy(gameObject);
    }
}
