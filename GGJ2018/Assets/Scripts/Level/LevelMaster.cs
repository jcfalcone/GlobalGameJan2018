﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LevelMaster : NetworkBehaviour 
{
	public static LevelMaster instance
	{
		get { return _instance; }//can also use just get;
		set { _instance = value; }//can also use just set;
	}

	//Creates a class variable to keep track of GameManger
	static LevelMaster _instance = null;

	[SerializeField]
	List<SpawnPointGizmos> spawnPoints = new List<SpawnPointGizmos>();

	[SerializeField]
	List<int> waveAI = new List<int>();
	
	[SerializeField]
	GameObject AIEnemys;
	
	[SerializeField]
	List<SpawnPointGizmos> enemySpawn = new List<SpawnPointGizmos>();

	[SyncVar ]
	int currWave;

	List<GameObject> enemys = new List<GameObject>();

	[SyncVar ]
	bool init = false;

	[SyncVar ]
	public bool startWave = false;
	

	// Use this for initialization
	void Awake () 
	{
		//check if GameManager instance already exists in Scene
		if(instance)
		{
			//GameManager exists,delete copy
			DestroyImmediate(gameObject);
			return;
		}
		else
		{
			//assign GameManager to variable "_instance"
			instance = this;
		}
	}

	void Update()
	{
		if(!this.init)
		{
			return;
		}

		for(int count = 0; count < this.enemys.Count; count++)
		{
			if(!this.enemys[count])
			{
				this.enemys.RemoveAt(count);
				break;
			}
		}

		if(this.enemys.Count <= 0 && !this.startWave)
		{
			StartCoroutine(this.StartAI());
		}
	}
	
	public Vector3 GetSpawnPosition(int _index)
	{

		Vector3 finalPos = this.spawnPoints[_index].transform.position;

		return finalPos;
	}

	public IEnumerator StartAI()
	{
		this.startWave = true;

		yield return new WaitForSeconds(4f);

		int currWaypoint = 0;

		if(this.currWave >= this.waveAI.Count)
		{
			this.currWave = 0;
		}

		for(int count = 0; count < this.waveAI[this.currWave]; count++)
		{
			GameObject enemy = Instantiate(this.AIEnemys, this.enemySpawn[currWaypoint].transform.position, Quaternion.identity);
			this.enemys.Add(enemy);
			NetworkServer.Spawn(enemy);

			yield return new WaitForSeconds(1f);

			currWaypoint++;

			if(currWaypoint >= this.enemySpawn.Count)
			{
				currWaypoint = 0;
			}
		}

		this.startWave = false;

		this.init = true;

		this.currWave++;
	}

	#region Command
    [Command]
    void CmdSendWave(int _wave)
    {
        this.RpcReceiveWave(_wave);
    }

    [ClientRpc]
    void RpcReceiveWave(int _wave)
    {
        this.currWave = _wave;
    }
	#endregion
}
