﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    // Available Audio clips to be called
    public enum AudioClips
    {
        // If not set
        Invalid = -1,

        // ENEMY
        BotRun, BotRun2, BotShot, Destruction, SpiderBotShot, TankBotFire, TankBotIdle, TankBotRun,
        // LEVEL
        ConfirmationSound, DoorOpening, ElevatorMoving, RoomingFreezer, VirusUploadSound,
        // PLAYER1
        Dashing, Idle, Jump, PlayerShot, Run, ShotMissed, ShotCooldown,
        // PLAYER2
        Dashing2, Idle2, Jump2, PlayerShot2, Run2, ShotMissed2, ShotCooldown2,
        // MUSIC
        MenuMusic, MenuMusic2, GameMusic1, GameMusic2, GameMusic3,
        // UIs
        CommenceTransmission, Keyboard, StartUp, Switch
    };

    #region SoundChannel Transforms

    //Child members of Sound Manager
    public Transform MusicSource;
    public Transform PlayerSource;
    public Transform Player2Source;
    public Transform EnemySource;
    public Transform LevelSource;
    public Transform UISource;

    #endregion

    #region Singleton

    public static SoundManager instance = null;

    
    // Use this for initialization
    void Awake()
    {
        //Instance creation
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }

        //Keep music consistent through scenes
        DontDestroyOnLoad(gameObject);
    }

    #endregion

    // Calling Reference: SoundManager.instance.Play(SoundManager.AudioClips."ClipName")
    public void Play(AudioClips clip)
    {
        if (clip == AudioClips.Invalid)
        {
            return;
        }

        // MUSIC (Set requested Clip to Channel)
        else if (clip == AudioClips.MenuMusic || clip == AudioClips.MenuMusic2 || 
            clip == AudioClips.GameMusic1 || clip == AudioClips.GameMusic2 ||
            clip == AudioClips.GameMusic3)
        {
            AudioSource music = MusicSource.GetComponent<AudioSource>();

            music.clip = GetClip(clip);

            music.Play();
        }
        
        // PLAYER (Set requested Clip to Channel) 
        else if (clip == AudioClips.Dashing || clip == AudioClips.Idle
                || clip == AudioClips.PlayerShot || clip == AudioClips.PlayerShot2
                || clip == AudioClips.ShotMissed || clip == AudioClips.Run
                || clip == AudioClips.Jump || clip == AudioClips.ShotCooldown)
        {
            AudioSource player = PlayerSource.GetComponent<AudioSource>();

            player.clip = GetClip(clip);

            player.Play();
        }

        // PLAYER 2 (Set requested Clip to Channel) 
        else if (clip == AudioClips.Dashing2 || clip == AudioClips.Idle2
                || clip == AudioClips.PlayerShot2 || clip == AudioClips.PlayerShot2
                || clip == AudioClips.ShotMissed2 || clip == AudioClips.Run2
                || clip == AudioClips.Jump2 || clip == AudioClips.ShotCooldown2)
        {
            AudioSource player = Player2Source.GetComponent<AudioSource>();

            player.clip = GetClip(clip);

            player.Play();
        }

        // ENEMIES (Set requested Clip to Channel)
        else if (clip == AudioClips.BotRun || clip == AudioClips.BotRun2 ||
            clip == AudioClips.Destruction || clip == AudioClips.SpiderBotShot ||
            clip == AudioClips.TankBotFire || clip == AudioClips.TankBotIdle ||
            clip == AudioClips.TankBotRun || clip == AudioClips.BotShot)
        {
            AudioSource grim = EnemySource.GetComponent<AudioSource>();

            grim.clip = GetClip(clip);

            grim.Play();
        }

        // LEVEL (Set requested Clip to Channel)
        else if (clip == AudioClips.ConfirmationSound || clip == AudioClips.DoorOpening ||
            clip == AudioClips.ElevatorMoving || clip == AudioClips.VirusUploadSound
            || clip == AudioClips.RoomingFreezer)
        {
            AudioSource sfx = LevelSource.GetComponent<AudioSource>();

            sfx.clip = GetClip(clip);

            sfx.Play();
        }


        // UI (Set requested Clip to Channel)
        else if (clip == AudioClips.CommenceTransmission || clip == AudioClips.Keyboard ||
            clip == AudioClips.StartUp || clip == AudioClips.Switch)
        {
            AudioSource UI = UISource.GetComponent<AudioSource>();

            UI.clip = GetClip(clip);

            UI.Play();
        }
    }
    

    /** Get the audio clip requested */
    public AudioClip GetClip(AudioClips clipName)
    {
        SoundManagerChannel smc = null;

        // MUSIC 
        if (clipName == AudioClips.MenuMusic || clipName == AudioClips.MenuMusic2 ||
            clipName == AudioClips.GameMusic1 || clipName == AudioClips.GameMusic2 ||
            clipName == AudioClips.GameMusic3)
        {
            smc = MusicSource.GetComponent<SoundManagerChannel>();

            if (clipName == AudioClips.MenuMusic)
            {
                return smc.Clips[0];
            }
            else if (clipName == AudioClips.MenuMusic2)
            {
                return smc.Clips[1];
            }
            else if (clipName == AudioClips.GameMusic1)
            {
                return smc.Clips[2];
            }
            else if (clipName == AudioClips.GameMusic2)
            {
                return smc.Clips[3];
            }
            else if (clipName == AudioClips.GameMusic3)
            {
                return smc.Clips[4];
            }
        }

        // PLAYER 1
        else if (clipName == AudioClips.Dashing || clipName == AudioClips.Idle
                || clipName == AudioClips.PlayerShot || clipName == AudioClips.Run
                || clipName == AudioClips.ShotMissed || clipName == AudioClips.Jump
                || clipName == AudioClips.ShotCooldown)
        {
            smc = PlayerSource.GetComponent<SoundManagerChannel>();

            if (clipName == AudioClips.Dashing)
            {
                return smc.Clips[0];
            }
            else if (clipName == AudioClips.Idle)
            {
                return smc.Clips[1];
            }
            else if (clipName == AudioClips.PlayerShot)
            {
                return smc.Clips[2];
            }
            else if (clipName == AudioClips.Run)
            {
                return smc.Clips[3];
            }
            else if (clipName == AudioClips.ShotMissed)
            {
                return smc.Clips[4];
            }
            else if (clipName == AudioClips.ShotCooldown)
            {
                return smc.Clips[5];
            }
        }

        // PLAYER 2
        else if (clipName == AudioClips.Dashing2 || clipName == AudioClips.Idle2
                || clipName == AudioClips.PlayerShot2 || clipName == AudioClips.Run2
                || clipName == AudioClips.ShotMissed2 || clipName == AudioClips.Jump2
                || clipName == AudioClips.ShotCooldown2)
        {
            smc = Player2Source.GetComponent<SoundManagerChannel>();

            if (clipName == AudioClips.Dashing2)
            {
                return smc.Clips[0];
            }
            else if (clipName == AudioClips.Idle2)
            {
                return smc.Clips[1];
            }
            else if (clipName == AudioClips.PlayerShot2)
            {
                return smc.Clips[2];
            }
            else if (clipName == AudioClips.Run2)
            {
                return smc.Clips[3];
            }
            else if (clipName == AudioClips.ShotMissed2)
            {
                return smc.Clips[4];
            }
            else if (clipName == AudioClips.ShotCooldown2)
            {
                return smc.Clips[5];
            }
        }

        // ENEMY
        else if (clipName == AudioClips.BotRun || clipName == AudioClips.BotRun2 ||
            clipName == AudioClips.Destruction || clipName == AudioClips.SpiderBotShot ||
            clipName == AudioClips.TankBotFire || clipName == AudioClips.TankBotIdle ||
            clipName == AudioClips.TankBotRun || clipName == AudioClips.BotShot)
        {
            smc = EnemySource.GetComponent<SoundManagerChannel>();

            if (clipName == AudioClips.BotRun)
            {
                return smc.Clips[0];
            }
            else if (clipName == AudioClips.BotRun2)
            {
                return smc.Clips[1];
            }
            else if (clipName == AudioClips.Destruction)
            {
                return smc.Clips[2];
            }
            else if (clipName == AudioClips.SpiderBotShot)
            {
                return smc.Clips[3];
            }
            else if (clipName == AudioClips.TankBotFire)
            {
                return smc.Clips[4];
            }
            else if (clipName == AudioClips.TankBotIdle)
            {
                return smc.Clips[5];
            }
            else if (clipName == AudioClips.TankBotRun)
            {
                return smc.Clips[6];
            }
            else if (clipName == AudioClips.BotShot)
            {
                return smc.Clips[7];
            }
        }

        // LEVEL
        else if (clipName == AudioClips.ConfirmationSound || clipName == AudioClips.DoorOpening ||
                clipName == AudioClips.ElevatorMoving || clipName == AudioClips.VirusUploadSound
                || clipName == AudioClips.RoomingFreezer)
        {
            smc = LevelSource.GetComponent<SoundManagerChannel>();

            if (clipName == AudioClips.ConfirmationSound)
            {
                return smc.Clips[0];
            }
            else if (clipName == AudioClips.DoorOpening)
            {
                return smc.Clips[1];
            }
            else if (clipName == AudioClips.ElevatorMoving)
            {
                return smc.Clips[2];
            }
            else if (clipName == AudioClips.VirusUploadSound)
            {
                return smc.Clips[3];
            }
            else if (clipName == AudioClips.RoomingFreezer)
            {
                return smc.Clips[4];
            }
        }

        else if (clipName == AudioClips.CommenceTransmission || clipName == AudioClips.Keyboard ||
            clipName == AudioClips.StartUp || clipName == AudioClips.Switch)
        {
            smc = UISource.GetComponent<SoundManagerChannel>();

            if (clipName == AudioClips.CommenceTransmission)
            {
                return smc.Clips[0];
            }
            else if (clipName == AudioClips.Keyboard)
            {
                return smc.Clips[1];
            }
            else if (clipName == AudioClips.StartUp)
            {
                return smc.Clips[2];
            }
            else if (clipName == AudioClips.Switch)
            {
                return smc.Clips[3];
            }
        }

        return null;
    }

    /* DEBUGGING
    private void Update()
    {
        //Music Preview (#1-7)
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                Play(AudioClips.GameMusic1);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                Play(AudioClips.GameMusic2);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                Play(AudioClips.GameMusic3);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                Play(AudioClips.GameMusic4);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                Play(AudioClips.GameMusic5);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                Play(AudioClips.GameMusic6);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha7))
            {
                Play(AudioClips.GameMusic7);
            }
    } */
}


/*  Sound Effects: (Cited)
 *  www.freesoundeffects.com
 *  www.soundbible.com
 *  www.freesounds.org
 *  www.AudioMicro.com
 *  
 *  
 *  Music: (Cited)
 *  www.purpleplanet.com
 */
