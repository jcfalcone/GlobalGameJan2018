﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour {

    [SerializeField]
    private GameObject pauseCanvas;

    private bool canvasActive;

    private void Update ()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            pauseCanvas.SetActive(SetCanvas());
        }

        if (canvasActive)
            if (Input.GetKeyDown(KeyCode.Return))
            {
                Debug.Log("Quit");
                Application.Quit();
                Debug.Break();
            }
	}

    private bool SetCanvas()
    {
        canvasActive = !canvasActive;
        return canvasActive;
    }
}
