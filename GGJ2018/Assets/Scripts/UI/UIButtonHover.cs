﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIButtonHover : MonoBehaviour
{
    [SerializeField]
    UIMainMenu mainMenu;

    [SerializeField]
    int indexBtn;

    public void OnMouseOver()
    {
        //If your mouse hovers over the GameObject with the script attached, output this message
        Debug.Log("Mouse is over GameObject.");
        this.mainMenu.SetIndex(this.indexBtn);

        SoundManager.instance.Play(SoundManager.AudioClips.Switch);
    }

    void OnMouseExit()
    {
        //The mouse is no longer hovering over the GameObject so output this message each frame
        Debug.Log("Mouse is no longer on GameObject.");
    }
}
