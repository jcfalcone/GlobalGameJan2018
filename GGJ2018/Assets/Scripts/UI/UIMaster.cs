﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIMaster : MonoBehaviour {

    #region Singleton Code

    public static UIMaster instance
    {
        get { return _instance; } // Can also use just get;
        set { _instance = value; } // Can also use just set;
    }

    /** Creates a class variable to keep track of GameManger */
    static UIMaster _instance = null;

    #endregion

    [SerializeField]
    private Canvas mainMenuCanvas;

    [SerializeField]
    private GameObject multiplayerBar;
    private bool multiplayerBarActive;

    /** Text to be changed */
    [SerializeField]
    private TextMeshProUGUI textmeshPro;

    /** Check and initialize instance if needed */
    private void Start()
    {
        /** check if GameManager instance already exists in Scene */
        if (instance)
        {
            /** UIMaster exists, delete copy */
            DestroyImmediate(gameObject);
            return;
        }
        else
        {
            /** assign UIMaster to variable "_instance" */
            instance = this;
        }
    }

    /** Debugging */
    private void Update()
    {
        /** Show/Hide Multiplayer Bar */
        if (Input.GetKeyDown(KeyCode.BackQuote))
        {
            SetMultiplayerBar();
        }
    }

    /** Show/Hide MultiplayerBar */
    public void SetMultiplayerBar()
    {
        /** Flip Boolean */
        multiplayerBarActive = !multiplayerBarActive;

        /** Show/Hide Panel */
        if (multiplayerBarActive)
            multiplayerBar.SetActive(true);
        else
            multiplayerBar.SetActive(false);
    }

    /** Update the text */
    public void UpdateMultiplayerBar(string message)
    {
        textmeshPro.text = message;
    }
}
