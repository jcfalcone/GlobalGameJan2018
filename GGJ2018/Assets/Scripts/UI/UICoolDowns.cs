﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICoolDowns : MonoBehaviour {

    [SerializeField]
    private Image m_content;
    [SerializeField]
    private Image m_contentEnergy;
    [SerializeField]
    private Image m_contentHealth;
    [SerializeField]
    private Image m_contentPlayerTwoHealth;

    [SerializeField]
    private Color m_fullColor;
    [SerializeField]
    private Color m_lowColor;

//    private float m_heatStatus;
//    private float m_heatMax = 1f;

//    public void HandleNewStatsHeat (float m_status) {
//
//        m_value = Mathf.RoundToInt (m_status / m_heatMax);
//        HandleBarHeat ();
//    }

    public void HandleBarHeat (float m_status) {
        m_content.fillAmount = (m_status);

        m_content.color = Color.Lerp (m_lowColor, m_fullColor, m_content.fillAmount);
    }

    public void HandleBarEnergy (float m_status) {
        m_contentEnergy.fillAmount = (m_status);

        m_contentEnergy.color = Color.Lerp (m_fullColor, m_lowColor, m_contentEnergy.fillAmount);
        Debug.Log("New fill: " + m_contentEnergy.fillAmount);
    }

    public void HandleBarHealth (float m_status) {
        m_contentHealth.fillAmount = (m_status / 100);

        m_contentHealth.color = Color.Lerp (m_fullColor, m_lowColor, m_contentHealth.fillAmount);
        Debug.Log("New fill: " + m_contentHealth.fillAmount);
    }

    public void HandleBarHealthTwo (float m_status) {
        m_contentPlayerTwoHealth.fillAmount = (m_status / 100);

        m_contentPlayerTwoHealth.color = Color.Lerp (m_fullColor, m_lowColor, m_contentPlayerTwoHealth.fillAmount);
        Debug.Log("New fill: " + m_contentPlayerTwoHealth.fillAmount);
    }
   
}
