﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class UIMainMenu : MonoBehaviour {

    /** Hide necessary panels when transmission commences */
    [SerializeField]
    private GameObject[] HideOnStart;

    /** Store reference to the currently Active GameObject from MainMenUCanvas */
    [SerializeField]
    private GameObject[] CurrActiveUI;

    /** Store reference to available images */
    [SerializeField]
    private GameObject[] BGImages;

    /** Store reference to player menu selection position */
    [SerializeField]
    private Transform[] selectionLocations;

    /** Store reference to the Selector sprites */
    [SerializeField]
    private TextMeshProUGUI selector;
    [SerializeField]
    private TextMeshProUGUI secondSelector;

    /** Reference to current position of selector */
    [SerializeField]
    private int index = 0;

    private float time;

    public void Awake()
    {
        SoundManager.instance.Play(SoundManager.AudioClips.MenuMusic);
        BGImages[0].SetActive(true);
    }

    /** Check for user input */
    public void Update()
    {

        time += Time.deltaTime;

        //Debug.Log("index: " + index);
        //Debug.Log("time: " + time);

        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S)
            || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            SoundManager.instance.Play(SoundManager.AudioClips.Switch);
        }

        if (CurrActiveUI[0].activeSelf)
        {
            /** DOWN: Set current Menu Image false, move index, set selector position, Set next menu image true */
            if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
            {
                if (index < 3)
                {
                    // Cycle Background image
                    BGImages[index].SetActive(false);
                    index++;
                    selector.transform.position = selectionLocations[index].position;
                    BGImages[index].SetActive(true);
                }
                else
                {
                    BGImages[index].SetActive(false);
                    index = 0;
                    selector.transform.position = selectionLocations[index].position;
                    BGImages[index].SetActive(true);
                }
            }

            /** UP: Set current Menu Image false, move index, set selector position, Set next menu image true */
            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
            {
                if (index > 0)
                {
                    BGImages[index].SetActive(false);
                    index--;
                    selector.transform.position = selectionLocations[index].position;
                    BGImages[index].SetActive(true);
                }
                else
                {
                    BGImages[index].SetActive(false);
                    index = 3;
                    selector.transform.position = selectionLocations[index].position;
                    BGImages[index].SetActive(true);
                }
            }

            /** ENTER: Execute corresponding function according to current selector position */
            if (Input.GetKeyDown(KeyCode.Return))
            {
                /** Start */
                if (index == 0)
                {
                    this.StartLobby();
                }

                /** Controls */
                if (index == 1)
                {
                    this.ShowControls();
                }

                /** Credits */
                if (index == 2)
                {
                    this.ShowCredits();
                }

                /** Exit */
                if (index == 3)
                {
                    this.Exit();
                }
            }
        }

        /** 
         * Controls menu is active
         */
        else if (CurrActiveUI[1].activeSelf)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                this.ReturnToMenuFromControls();
            }
        }

        /**
         * Credits menu is active
         */
        if (CurrActiveUI[2].activeSelf)
        {
            /** DOWN: Set current Menu Image false, move index, set selector position, Set next menu image true */
            if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
            {
                if (index == 4)
                {
                    // Cycle Background image
                    BGImages[index].SetActive(false);
                    index++;
                    secondSelector.transform.position = selectionLocations[index].position;
                    BGImages[index].SetActive(true);
                }
                else
                {
                    BGImages[index].SetActive(false);
                    index = 4;
                    secondSelector.transform.position = selectionLocations[index].position;
                    BGImages[index].SetActive(true);
                }
            }

            /** UP: Set current Menu Image false, move index, set selector position, Set next menu image true */
            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
            {
                if (index == 5)
                {
                    BGImages[index].SetActive(false);
                    index--;
                    secondSelector.transform.position = selectionLocations[index].position;
                    BGImages[index].SetActive(true);
                }
                else
                {
                    BGImages[index].SetActive(false);
                    index = 5;
                    secondSelector.transform.position = selectionLocations[index].position;
                    BGImages[index].SetActive(true);
                }
            }

            /** ENTER: Execute corresponding function according to current selector position */
            if (Input.GetKeyDown(KeyCode.Return) && time > 0.25f)
            {
                /** Further Credits */
                if (index == 4)
                {
                    this.ShowFurtherCredits();
                }

                /** Return */
                if (index == 5)
                {
                    this.ReturnToMenuFromCredits();
                }
            }
        }

        // Further Credits
        if (CurrActiveUI[3].activeSelf)
        {
            /** ENTER: Execute corresponding function according to current selector position */
            if (Input.GetKeyDown(KeyCode.Return) && time > 0.25f)
            {
                this.ShowCredits();
            }
        }
    }

    /** Set index according to button clicked to update selector*/
    public void SetIndex(int _newIndex)
    {
        BGImages[index].SetActive(false);
        index = _newIndex;
        if (CurrActiveUI[0].activeSelf)
            selector.transform.position = selectionLocations[index].position;
        else if (CurrActiveUI[2].activeSelf)
            secondSelector.transform.position = selectionLocations[index].position;
        BGImages[index].SetActive(true);
    }

    /** Start Game */
    public void StartLobby()
    {
        SoundManager.instance.Play(SoundManager.AudioClips.CommenceTransmission);
        SoundManager.instance.Play(SoundManager.AudioClips.GameMusic3);
        // Work with Network Manager to Start the game
        NetworkLobby.instance.FindRoom();

        foreach(GameObject Object in HideOnStart)
        {
            Object.SetActive(false);
        }
    }

    /** Controls */
    public void ShowControls()
    {
        SoundManager.instance.Play(SoundManager.AudioClips.CommenceTransmission);
        // Deactivate Menu, activate Controls Panels
        CurrActiveUI[0].SetActive(false);
        CurrActiveUI[1].SetActive(true);
    }

    /** Credits */
    public void ShowCredits()
    {
        SoundManager.instance.Play(SoundManager.AudioClips.CommenceTransmission);

        time = 0;

        index = 4;
        secondSelector.transform.position = selectionLocations[index].position;

        // Deactivate Menu, Deactivate Further Credits, activate Credits Panels
        CurrActiveUI[2].SetActive(true);
        CurrActiveUI[0].SetActive(false);
        CurrActiveUI[3].SetActive(false);
    }

    /** Further Credits */
    public void ShowFurtherCredits()
    {
        SoundManager.instance.Play(SoundManager.AudioClips.CommenceTransmission);

        time = 0;

        Debug.Log("Further Credits");

        // Deactivate Credits, activate Further Credits Panels
        CurrActiveUI[2].SetActive(false);
        CurrActiveUI[3].SetActive(true);

        Debug.Log("Past Further Credits");
    }

    /** Exit Game */
    public void Exit()
    {
        Debug.Log("Quit");
        Application.Quit();
        Debug.Break();
    }

    /** Controls Return to Menu*/
    public void ReturnToMenuFromControls()
    {
        SoundManager.instance.Play(SoundManager.AudioClips.CommenceTransmission);

        CurrActiveUI[1].SetActive(false);
        CurrActiveUI[0].SetActive(true);
    }

    /** Credits Return to Menu*/
    public void ReturnToMenuFromCredits()
    {
        SoundManager.instance.Play(SoundManager.AudioClips.CommenceTransmission);

        CurrActiveUI[0].SetActive(true);
        CurrActiveUI[2].SetActive(false);
        BGImages[5].SetActive(false);
        BGImages[4].SetActive(true);
        index = 2;
        selector.transform.position = selectionLocations[index].position;
        BGImages[index].SetActive(true);
    }
}
