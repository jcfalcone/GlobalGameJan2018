﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace AI
{
    public enum Actions
    {
        Invalid = -1,
        Walk,
        Attack,
        Size
    }

    public class AgentPerceptionComponent : MonoBehaviour
    {
        AIAgent agent;
        Actions action;

        NavMeshPath path;
        NavMeshPath pathCore;
        NavMeshPath pathPlayer1;
        NavMeshPath pathPlayer2;

        public void Init(AIAgent agent)
        {
            this.agent = agent;
            path = new NavMeshPath();
            pathCore = new NavMeshPath();
            pathPlayer1 = new NavMeshPath();
            pathPlayer2 = new NavMeshPath();
        }
        
        public void Tick(float deltaTime)
        {
            float finalUtil = 0.0f;
            float util;

            action = Actions.Invalid;
            
            util = CalculateUtil(CalculateAttack());
            if (util > finalUtil) { finalUtil = util; action = Actions.Attack; }

            util = CalculateUtil(CalculateDistanceToTarget());
            if (util > finalUtil) { finalUtil = util; action = Actions.Walk; }

            SetupAction(action);
        }

        float CalculateUtil(List<float> utils)
        {
            float numConsiderations = (float)utils.Count;
            float modFactor = 1.0f - (1.0f / numConsiderations);
            float UtilScore = 0f;
            foreach (float utilScore in utils)
            {
                if (UtilScore == 0f)
                {
                    UtilScore = 1f;
                }

                float makeUpVal = (1.0f - utilScore) * modFactor;
                float finalScore = utilScore + (makeUpVal * utilScore);
                UtilScore *= finalScore;
            }
            return UtilScore;
        }

        void SetupAction(Actions action)
        {
            

            switch (action)
            {
                case Actions.Walk:
                    float w1 = (agent.core != null) ? Vector3.Distance(agent.core.transform.position, agent.transform.position) : Mathf.Infinity;
                    float w2 = (agent.player1 != null) ? Vector3.Distance(agent.player1.transform.position, agent.transform.position) : Mathf.Infinity;
                    float w3 = (agent.player2 != null) ? Vector3.Distance(agent.player2.transform.position, agent.transform.position) : Mathf.Infinity;

                    float walkable = Mathf.Min(w1, w2, w3);

                    path.ClearCorners();

                    if (walkable == w1)
                    {
                        NavMesh.CalculatePath(transform.position, agent.core.transform.position, NavMesh.AllAreas, this.path);
                        agent.targetObject = agent.core;
                    }
                    else if (walkable == w2)
                    {
                        NavMesh.CalculatePath(transform.position, agent.player1.transform.position, NavMesh.AllAreas, this.path);
                        agent.targetObject = agent.player1;
                    }
                    else
                    {
                        NavMesh.CalculatePath(transform.position, agent.player2.transform.position, NavMesh.AllAreas, this.path);
                        agent.targetObject = agent.player2;
                    }

                    if(path.corners.Length > 0)
                    {
                        agent.target = path.corners[1];
                    }
                    else
                    {
                        agent.target = agent.targetObject.transform.position;
                    }

                    agent.SetMoveH(1.0f);

                    agent.SetHasAction(true);
                    agent.SetIsMoving(true);
                    break;

                case Actions.Attack:

                    float d1 = (agent.core != null) ?  Vector3.Distance(agent.core.transform.position, agent.transform.position) : Mathf.Infinity;
                    float d2 = (agent.player1 != null) ?  Vector3.Distance(agent.player1.transform.position, agent.transform.position) : Mathf.Infinity;
                    float d3 = (agent.player2 != null) ? Vector3.Distance(agent.player2.transform.position, agent.transform.position) : Mathf.Infinity;

                    float res = Mathf.Min(d1, d2, d3);

                    if(res == d1)
                    {
                        agent.target = agent.core.transform.position;
                        agent.targetObject = agent.core;
                    }
                    else if(res == d2)
                    {
                        agent.target = agent.player1.transform.position;
                        agent.targetObject = agent.player1;
                    }
                    else
                    {
                        agent.target = agent.player2.transform.position;
                        agent.targetObject = agent.player2;
                    }

                    agent.SetHasAction(true);
                    agent.SetIsAttacking(true);
                    break;
            }
        }

        List<float> CalculateDistanceToTarget()
        {
            List<float> util = new List<float>();

            float d1 = (agent.core != null) ? Vector3.Distance(agent.core.transform.position, agent.transform.position) : Mathf.Infinity;
            float d2 = (agent.player1 != null) ? Vector3.Distance(agent.player1.transform.position, agent.transform.position) : Mathf.Infinity;
            float d3 = (agent.player2 != null) ? Vector3.Distance(agent.player2.transform.position, agent.transform.position) : Mathf.Infinity;

            float res = Mathf.Min(d1, d2, d3);

            pathCore.ClearCorners();
            pathPlayer1.ClearCorners();
            pathPlayer2.ClearCorners();

            if (res == d1)
            {
                float distance = Vector3.Distance(agent.core.transform.position, agent.transform.position);
                float value = Mathf.Clamp01((distance / agent.attackDistance));
                util.Add(value);
            }
            else if (res == d2)
            {
                float distance = Vector3.Distance(agent.player1.transform.position, agent.transform.position);
                float value = Mathf.Clamp01((distance / agent.attackDistance));
                util.Add(value);
            }
            else if(res == d3)
            {
                float distance = Vector3.Distance(agent.player2.transform.position, agent.transform.position);
                float value = Mathf.Clamp01((distance / agent.attackDistance));
                util.Add(value);
            }
            else
                util.Add(0.0f);

            return util;
        }
        
        List<float> CalculateAttack()
        {
            List<float> util = new List<float>();

            if (this.agent.targetObject != null)
            {
                float distance = Vector3.Distance(agent.targetObject.transform.position, agent.transform.position);
                float value = Mathf.Clamp01((agent.attackDistance / distance));
                util.Add(value);
            }
            else
                util.Add(0.0f);
            
            return util;
        }
    }
}