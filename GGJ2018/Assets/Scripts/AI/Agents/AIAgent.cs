﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class AIAgent : Pawn
    {
        [Header("Essentials")]
        [SerializeField] Rigidbody rigid;
        [SerializeField] Animator anim;

        [Header("Stats")]
        public Vector3 target;
        public GameObject targetObject;
        public GameObject player1;
        public GameObject player2;
        public GameObject core;
        [SerializeField] float moveSpeed;
        [SerializeField] float moveH;
        public float attackDistance;
        [SerializeField] int damage;
        [SerializeField] List<ParticleSystem> guns;

        [Header("Status")]
        [SerializeField] bool hasAction;
        [SerializeField] bool isMoving;
        [SerializeField] bool isAttacking;
        [SerializeField] bool useSpecial;

        StateManager stateManager;
        AgentPerceptionComponent perception;
        
        void Start()
        {
            this.core = GameObject.FindGameObjectWithTag("Core");
            this.player1 = GameObject.FindGameObjectWithTag("Player1");
            this.player2 = GameObject.FindGameObjectWithTag("Player2");

            health = 100;

            stateManager = gameObject.AddComponent<StateManager>();
            stateManager.Init(this);

            perception = gameObject.AddComponent<AgentPerceptionComponent>();
            perception.Init(this);

            targetObject = core;
        }
        
        void Update()
        {
            float delta = Time.deltaTime;

            OnGround();

            if (!hasAction)
            {
                perception.Tick(delta);
            }

            stateManager.Tick(delta);
        }

        void OnGround()
        {
            float offset = 1.5f;
            float distance = offset * 2.0f;

            Vector3 origin = transform.position;
            origin.y += offset;

            Vector3 direction = -Vector3.up;

            Debug.DrawRay(origin, direction * distance, Color.green);
            RaycastHit hit;
            if (Physics.Raycast(origin, direction, out hit, distance))
            {
                transform.position = hit.point;
            }
        }

        public void CleanAgent()
        {
            target = Vector3.negativeInfinity;

            moveH = 0.0f;

            hasAction = false;
            isMoving = false;
            isAttacking = false;
            useSpecial = false;
        }

        public void FireGun(int gun)
        {
            if(FireCastTarget())
                targetObject.GetComponentInChildren<Pawn>().ApplyDamage(damage);

            guns[gun].Play();
        }

        bool FireCastTarget()
        {
            float offset = 1.5f;
            float distance = attackDistance;

            Vector3 origin = transform.position;
            origin.y += offset;

            Vector3 direction = transform.forward;

            Debug.DrawRay(origin, direction * distance, Color.red);
            RaycastHit hit;
            if(Physics.Raycast(origin, direction, out hit, distance))
            {
                if (hit.transform.CompareTag("Player1") || hit.transform.CompareTag("Player2") || hit.transform.CompareTag("Core"))
                    return true;
            }

            return false;
        }

        // GETTERS
        public Rigidbody GetAgentRigidbody() { return this.rigid; }
        public Animator GetAgentAnim() { return this.anim; }
        public float GetMoveSpeed() { return this.moveSpeed; }
        public float GetMoveH() { return this.moveH; }
        public int GetDamage() { return this.damage; }

        public bool GetHasAction() { return this.hasAction; }
        public bool GetIsMoving() { return this.isMoving; }
        public bool GetIsAttacking() { return this.isAttacking; }
        public bool GetUseSpecial() { return this.useSpecial; }

        public bool GetIsAlive() { return !this.isAlive; }

        //SETTERS
        public void SetTarget(Vector3 target) { this.target = target; }
        public void SetMoveH(float value) { this.moveH = value; }

        public void SetHasAction(bool value) { this.hasAction = value; }
        public void SetIsMoving(bool value) { this.isMoving = value; }
        public void SetIsAttacking(bool value) { this.isAttacking = value; }
        public void SetUseSpecial(bool value) { this.useSpecial = value; }
    }
}