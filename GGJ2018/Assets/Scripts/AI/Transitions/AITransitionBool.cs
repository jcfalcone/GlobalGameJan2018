﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class AITransitionBool : AITransitionBase
    {
        public AITransitionBool(Method method, StateName nextState) : base(method, nextState) { }

        public override StateName RunTransition()
        {
            if (method())
                return nextState;

            return StateName.Invalid;
        }
    }
}