﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public delegate bool Method();

    public abstract class AITransitionBase
    {
        protected Method method;

        protected StateName nextState;

        public AITransitionBase( Method method, StateName nextState )
        {
            this.method = method;

            this.nextState = nextState;
        }

        public abstract StateName RunTransition();
    }
}