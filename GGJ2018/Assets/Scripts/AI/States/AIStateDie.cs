﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class AIStateDie : AIStateBase
    {
        Animator anim;

        string dieAnim = "Die_Animation";

        public AIStateDie(AIAgent agent, StateName statename) : base(agent, statename)
        {
            anim = agent.GetAgentAnim();
        }

        public override void OnStateEnter()
        {
            anim.Play(dieAnim);
        }

        public override StateName OnStateUpdate(float deltaTime)
        {
            return StateName.Invalid;
        }

        public override void OnStateExit()
        {

        }
    }
}