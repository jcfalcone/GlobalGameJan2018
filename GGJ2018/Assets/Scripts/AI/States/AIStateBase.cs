﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public abstract class AIStateBase
    {
        protected AIAgent agent;
        protected StateName name;

        protected List<AITransitionBase> transitions;

        public AIStateBase(AIAgent agent, StateName name)
        {
            this.agent = agent;

            this.name = name;

            transitions = new List<AITransitionBase>();
        }

        public abstract void OnStateEnter();

        public abstract StateName OnStateUpdate(float deltaTime);

        public abstract void OnStateExit();

        public void SetStateName(StateName name) { this.name = name; }
        public StateName GetStateName() { return this.name; }
    }
}