﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class AIStateRotate : AIStateBase
    {
        float turnSpeed = 8.0f;

        public AIStateRotate(AIAgent agent, StateName statename) : base(agent, statename)
        {
            transitions.Add(new AITransitionBool(agent.GetIsMoving, StateName.Walk));
            transitions.Add(new AITransitionBool(agent.GetIsAttacking, StateName.Attack));
            transitions.Add(new AITransitionBool(agent.GetIsAlive, StateName.Die));
        }

        public override void OnStateEnter()
        {
        }

        public override StateName OnStateUpdate(float deltaTime)
        {
            StateName next = StateName.Invalid;

            Vector3 destination = agent.target;
            Vector3 origin = agent.transform.position;

            destination.y = origin.y = 0.0f;

            Vector3 direction = (destination - origin).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(direction);

            agent.transform.rotation = Quaternion.Slerp(agent.transform.rotation, lookRotation, turnSpeed * deltaTime);

            foreach (AITransitionBase transition in transitions)
            {
                next = transition.RunTransition();
                if (next != StateName.Invalid)
                    break;
            }

            return next;
        }

        public override void OnStateExit()
        {
        }
    }
}