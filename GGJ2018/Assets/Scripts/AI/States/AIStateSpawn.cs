﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class AIStateSpawn : AIStateBase
    {
        Animator anim;

        string spawnAnim = "Spawn_Animation";

        bool isDone;

        public AIStateSpawn(AIAgent agent, StateName statename) : base(agent, statename)
        {
            anim = this.agent.GetAgentAnim();

            transitions.Add(new AITransitionBool(this.GetIsDone, StateName.Idle));
            transitions.Add(new AITransitionBool(agent.GetIsAlive, StateName.Die));
        }

        public override void OnStateEnter()
        {
            this.isDone = false;

            anim.Play(spawnAnim);
        }

        public override StateName OnStateUpdate(float deltaTime)
        {
            StateName next = StateName.Invalid;

            isDone = anim.GetBool("IsSpawned");

            foreach (AITransitionBase transition in transitions)
            {
                next = transition.RunTransition();
                if (next != StateName.Invalid)
                    break;
            }

            return next;
        }

        public override void OnStateExit()
        {
            
        }

        public bool GetIsDone() { return this.isDone; }
    }
}