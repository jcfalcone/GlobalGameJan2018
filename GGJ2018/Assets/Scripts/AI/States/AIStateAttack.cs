﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class AIStateAttack : AIStateBase
    {
        Rigidbody rigid;
        Animator anim;

        string attackAnim = "Attack_Animation";

        bool isDone;

        public AIStateAttack(AIAgent agent, StateName statename) : base(agent, statename)
        {
            rigid = agent.GetAgentRigidbody();
            anim = agent.GetAgentAnim();

            transitions.Add(new AITransitionBool(this.GetIsDone, StateName.Idle));
        }

        public override void OnStateEnter()
        {
            this.isDone = false;

            anim.SetFloat("Horizontal", 0.0f);

            rigid.velocity = Vector3.zero;

            anim.Play(attackAnim);
        }

        public override StateName OnStateUpdate(float deltaTime)
        {
            StateName next = StateName.Invalid;

            isDone = anim.GetBool("IsAttacking");
            
            foreach (AITransitionBase transition in transitions)
            {
                next = transition.RunTransition();
                if (next != StateName.Invalid)
                    break;
            }

            return next;
        }

        public override void OnStateExit()
        {

        }

        public bool GetIsDone() { return this.isDone; }
    }
}