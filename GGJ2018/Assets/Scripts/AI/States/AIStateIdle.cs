﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class AIStateIdle : AIStateBase
    {
        Rigidbody rigid;
        Animator anim;

        float horizontal = 0.0f;
        float moveAmount = 0.0f;

        float moveSpeed = 5.0f;

        public AIStateIdle(AIAgent agent, StateName statename) : base(agent, statename)
        {
            rigid = agent.GetAgentRigidbody();
            anim = agent.GetAgentAnim();

            transitions.Add(new AITransitionBool(agent.GetHasAction, StateName.Rotate));
            transitions.Add(new AITransitionBool(agent.GetIsAlive, StateName.Die));
        }

        public override void OnStateEnter()
        {
            agent.CleanAgent();

            moveSpeed = agent.GetMoveSpeed();
        }

        public override StateName OnStateUpdate(float deltaTime)
        {
            StateName next = StateName.Invalid;

            float animH = anim.GetFloat("Horizontal");

            horizontal = Mathf.Lerp(animH, 0.0f, moveSpeed* deltaTime);
            
            anim.SetFloat("Horizontal", horizontal);

            moveAmount = Mathf.Clamp01(Mathf.Abs(horizontal));

            ManageVelocity();

            foreach (AITransitionBase transition in transitions)
            {
                next = transition.RunTransition();
                if (next != StateName.Invalid)
                    break;
            }

            return next;
        }

        public override void OnStateExit()
        {
        }

        void ManageVelocity()
        {
            Vector3 forwardVelocity = agent.transform.forward * horizontal;

            Vector3 finalVelocity = (forwardVelocity).normalized;

            rigid.velocity = finalVelocity * (moveSpeed * moveAmount);
        }
    }
}