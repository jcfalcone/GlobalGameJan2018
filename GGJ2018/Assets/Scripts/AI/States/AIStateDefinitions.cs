﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public enum StateName
    {
        Invalid = -1,
        Spawn,
        Idle,
        Rotate,
        Walk,
        Attack,
        Die,
        Size
    }
}