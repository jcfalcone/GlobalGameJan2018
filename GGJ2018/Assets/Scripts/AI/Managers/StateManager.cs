﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class StateManager : MonoBehaviour
    {
        List<AIStateBase> states;
        AIStateBase currentState = null;
        [SerializeField] StateName current;

        // Use this for initialization
        public void Init(AIAgent agent)
        {
            states = new List<AIStateBase>();

            states.Add(new AIStateSpawn(agent, StateName.Spawn));
            states.Add(new AIStateIdle(agent, StateName.Idle));
            states.Add(new AIStateRotate(agent, StateName.Rotate));
            states.Add(new AIStateWalk(agent, StateName.Walk));
            states.Add(new AIStateAttack(agent, StateName.Attack));
            states.Add(new AIStateDie(agent, StateName.Die));

            SetCurrentState(StateName.Spawn);
        }

        // Update is called once per frame
        public void Tick( float deltaTime )
        {
            if(currentState != null)
            {
                SetCurrentState(currentState.OnStateUpdate(deltaTime));
            }
        }

        public void SetCurrentState(StateName statename)
        {
            if (statename == StateName.Invalid) return;
            if (currentState != null && currentState.GetStateName() == statename) return;

            if(currentState != null)
                currentState.OnStateExit();
            currentState = null;

            foreach (AIStateBase state in states)
            {
                if (state.GetStateName() == statename)
                {
                    currentState = state;
                    currentState.OnStateEnter();
                    current = statename;
                    break;
                }
            }
        }
    }
}