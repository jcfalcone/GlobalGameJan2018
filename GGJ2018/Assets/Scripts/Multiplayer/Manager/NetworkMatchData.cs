﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LobbyPlayerInfo 
{
    public NetLobbyPlayer lobbyPlayer;
}

public class NetworkMatchData 
{

    public string name;
    public int map;
    public int maxPlayers;
    public int status;

    public NetworkMatchData()
    {
        this.name = System.DateTime.Now.ToString();
        this.map = 0;
        this.maxPlayers = 2;
        this.status = 0;
    }

    public string GetNetworkName()
    {
        string tempName = this.name;
        tempName += ".M.";
        tempName += this.map.ToString();
        tempName += ".S.";
        tempName += this.status.ToString();

        return tempName;
    }

    static public string StrToMap(string _map)
    {
        int mapNum;

        int.TryParse(_map,out mapNum);

        switch (mapNum)
        {
            case 0:
                return "Level1";
        }

        return "Error";
    }

    static public NetworkMatchData ParseData(string dataStr)
    {
        NetworkMatchData netData = new NetworkMatchData();

        string[] data = dataStr.Split(new string[]{ ".M." }, System.StringSplitOptions.RemoveEmptyEntries);

        netData.name = data[0];

        dataStr = data[1];

        data = dataStr.Split(new string[]{ ".S." }, System.StringSplitOptions.RemoveEmptyEntries);

        netData.map =  int.Parse(data[0]);
        netData.status = int.Parse(data[1]);

        return netData;
    }
}
