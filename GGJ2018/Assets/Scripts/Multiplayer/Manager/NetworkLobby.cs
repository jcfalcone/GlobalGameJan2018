﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class NetworkLobby : NetworkLobbyManager 
{
	public static NetworkLobby instance
	{
		get { return _instance; }//can also use just get;
		set { _instance = value; }//can also use just set;
	}

	//Creates a class variable to keep track of GameManger
	static NetworkLobby _instance = null;

	[SerializeField]
	int RoomSize;

    private NetworkManager NetManager;
	public List<LobbyPlayerInfo> players =  new List<LobbyPlayerInfo>();
	public LobbyPlayerInfo localPlayer;

	// Use this for initialization
	void Start () 
	{
		//check if GameManager instance already exists in Scene
		if(instance)
		{
			//GameManager exists,delete copy
			DestroyImmediate(gameObject);
			return;
		}
		else
		{
			//assign GameManager to variable "_instance"
			instance = this;
		}

		this.NetManager = NetworkManager.singleton;

		DontDestroyOnLoad(gameObject);
	}

	public void FindRoom()
	{
		if(this.NetManager.matchMaker == null)
		{
			this.NetManager.StartMatchMaker();
		}

		this.NetManager.matchMaker.ListMatches(0, 100, string.Empty, true, 0, 0, this.OnMatchList);

        if(UIMaster.instance)
        {
            UIMaster.instance.SetMultiplayerBar();
            UIMaster.instance.UpdateMultiplayerBar("Searching for a Room...");
        }
	}
	
	public override void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
	{
		if (success)
		{
            Debug.Log(extendedInfo);

			if (matches.Count <= 0)
			{
				this.StartRoom();
				return;
			}

			foreach (MatchInfoSnapshot matchInfo in matches)
			{
				if (matchInfo.currentSize < matchInfo.maxSize)
				{
					this.JoinRoom(matchInfo);
					return;
				}
			}
		}
	}

	public void JoinRoom(MatchInfoSnapshot matchInfo)
	{
		NetworkMatchData data = new NetworkMatchData();
		data = NetworkMatchData.ParseData(matchInfo.name);
		data.maxPlayers = matchInfo.maxSize; 

		Debug.Log("Joining Room - "+matchInfo.name+" - "+matchInfo.currentSize+" / "+matchInfo.maxSize);

		this.NetManager.matchMaker.JoinMatch(matchInfo.networkId, "", "", "", 0, 0, this.JoinMatch);

		NetworkMaster.instance.currMatch = data;

        if (UIMaster.instance)
        {
            UIMaster.instance.UpdateMultiplayerBar("Joining Game!!!");
        }
    }
    	
	public void StartRoom()
	{
		Debug.Log("Creating Room");

		NetworkMatchData data = new NetworkMatchData();
		NetworkMaster.instance.currMatch = data;

		//this.netManager.matchMaker.CreateMatch(data.GetNetworkName(), (uint)data.maxPlayers, true, "", "", "", 0, 0, this.netManager.OnMatchCreate);
		this.NetManager.matchMaker.CreateMatch(data.GetNetworkName(), (uint)data.maxPlayers, true, "", "", "", 0, 0, this.CreateMatch);
        
        if (UIMaster.instance)
        {
            UIMaster.instance.UpdateMultiplayerBar("Creating Game...");
        }
    }

	void JoinMatch(bool _success, string _extendedInfo, MatchInfo _match)
	{
		if (_success)
		{
			NetworkMaster.instance.hosting = false;

			StartClient(_match);

			Debug.Log("In Lobby!");

            if (UIMaster.instance)
            {
                UIMaster.instance.UpdateMultiplayerBar("Waiting for Players...");
            }
        }
	}

	void CreateMatch(bool success, string extendedInfo, MatchInfo match)
	{
		if (success)
		{
			NetworkServer.Listen(match, 9000);
			this.StartHost(match);

			NetworkMaster.instance.hosting = true;

            if (UIMaster.instance)
            {
                UIMaster.instance.UpdateMultiplayerBar("Waiting for Players...");
            }
        }
	}

	public void LeaveRoom()
	{
		MatchInfo info = NetManager.matchInfo;
		this.NetManager.matchMaker.DropConnection(info.networkId, info.nodeId, 0, NetManager.OnDropConnection);
		this.NetManager.StopHost();
		StopClient();
		StopMatchMaker();
        
        if (UIMaster.instance)
        {
            UIMaster.instance.SetMultiplayerBar();
        }
    }

	public override void OnClientDisconnect(NetworkConnection conn)
	{
		base.OnClientDisconnect(conn);
	}

	public override void OnServerDisconnect(NetworkConnection conn)
	{
		base.OnServerDisconnect(conn);
		StartCoroutine(CheckRemovedPlayer());
	}

	IEnumerator CheckRemovedPlayer()
	{
		yield return new WaitForSeconds(1);
		LobbyPlayerInfo _toRemove = null;

		for (int count = 0; count < this.players.Count; count++)
		{
			if (this.players[count].lobbyPlayer == null)
			{
				_toRemove = this.players[count];
				break;
			}
		}

		if (this.players.Contains(_toRemove))
		{
			this.players.Remove(_toRemove);
		}
	}
	public void AddPlayerLobby(NetLobbyPlayer _player)
	{
		LobbyPlayerInfo temp = this.players.Find(player => player.lobbyPlayer == _player);

		if (temp != null)
		{
			return;
		}

		LobbyPlayerInfo info = new LobbyPlayerInfo();
		info.lobbyPlayer = _player;

		this.players.Add(info);

		if (_player.isLocalPlayer)
		{
			this.localPlayer = info;
		}

		if (this.players.Count >= this.RoomSize)
        {

            if (UIMaster.instance)
            {
                UIMaster.instance.SetMultiplayerBar();
            }

            this.StartGame();
		}
	}

	public void StartGame()
    {
        foreach (LobbyPlayerInfo info in this.players)
		{
			if (info.lobbyPlayer.isLocalPlayer)
			{
				info.lobbyPlayer.SendReadyToBeginMessage();
			}
		}
	}

	public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
	{
		//bool parentReturn = base.OnLobbyServerSceneLoadedForPlayer(lobbyPlayer, gamePlayer);
		MultiplayerSync sync = gamePlayer.GetComponent<MultiplayerSync>();
		NetLobbyPlayer lobbyP = lobbyPlayer.GetComponent<NetLobbyPlayer>();

		LobbyPlayerInfo info = this.players.Find(x=> x.lobbyPlayer == lobbyP);

		sync.Init(this.players.IndexOf(info));

		//
		//sync.mainWeapon = info.mainWeapon;
		//sync.secWeapon = info.secWeapon;

		return true;
	} 

	#region GetSet
	public LobbyPlayerInfo GetPlayer(uint _playerId)
	{
		return this.players.Find(player => player.lobbyPlayer.netId.Value == _playerId);
	}

	public LobbyPlayerInfo GetLocalPlayer()
	{
		return this.localPlayer;
	}
	#endregion
}
