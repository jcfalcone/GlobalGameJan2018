﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkMaster: MonoBehaviour 
{
	public static NetworkMaster instance
	{
		get { return _instance; }//can also use just get;
		set { _instance = value; }//can also use just set;
	}

	public NetworkMatchData currMatch;

	public bool hosting;

	public string playerPrefix;

	//Creates a class variable to keep track of GameManger
	static NetworkMaster _instance = null;
	
	// Use this for initialization
	void Awake () 
	{
		//check if GameManager instance already exists in Scene
		if(instance)
		{
			//GameManager exists,delete copy
			DestroyImmediate(gameObject);
			return;
		}
		else
		{
			//assign GameManager to variable "_instance"
			instance = this;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
