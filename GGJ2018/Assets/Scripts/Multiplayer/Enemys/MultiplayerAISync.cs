﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using AI;

public class MultiplayerAISync : NetworkBehaviour 
{


    public int sendRate = 4;
    public float movementThreshold = 0.1f;
    public float angleThreshold = 5; 
	
    public bool isInit;
	public bool isLocal;

	AIAgent agent;
    DummyAIController dummyControl;

	[SerializeField]
	Animator animator;

	[SerializeField]
	Rigidbody rigidbody;


    #region SyncVars
    bool prevShooting;

    string prevTarget;

    int prevHealth;

    float prevHorizontal;
    #endregion

	int countRate = 0;
	// Use this for initialization
	void Start () 
	{
        if (isServer)
        {
            this.InitAIController();
        }
        else
        {
            this.InitDummyController();
        }
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!this.isInit)
		{
			return;
		}

        if (!this.isLocal)
        {
            this.DummyTick();
        }

        if (this.countRate < this.sendRate)
        {
            this.countRate++;
            return;
        }

        if (this.isLocal)
        {
            this.LocalPlayerTick();
        }

        this.countRate = 0;
	}

    void LateUpdate()
    {
        if (!this.isInit)
        {
            return;
        }

        if (this.countRate != sendRate)
        {
            return;
        }

        if (this.isLocal)
        {
            this.LocalAILateTick();
        }
    }

    void DummyTick()
    {
        this.dummyControl.Tick();
    }

	void LocalPlayerTick()
	{
        float horizontal = this.agent.GetAgentAnim().GetFloat("Horizontal");

        if(this.prevShooting != this.agent.GetIsAttacking())
        {
            this.CmdSendShooting(this.agent.GetIsAttacking());
        }

        if(this.prevHorizontal != horizontal)
        {
            this.CmdSendHorizontal(horizontal);
        }

        if(this.prevHealth != this.agent.health)
        {
            this.CmdSendHealth(this.agent.health);
        }
        
        if(this.agent.targetObject)
        {
            this.CmdSendTarget(this.agent.targetObject.tag);
            this.prevTarget = this.agent.targetObject.tag;
        }

        if(!this.agent.isAlive)
        {
            this.CmdSendDie();
        }

        this.prevHorizontal = horizontal;
        this.prevShooting = this.agent.GetIsAttacking();
        this.prevHealth = this.agent.health;
	}

	void LocalAILateTick()
    {
        this.CmdSendPosition(this.transform.position, this.transform.rotation);
	}

	public void InitAIController()
    {
		Destroy(GetComponent<DummyAIController>());
        this.agent = GetComponent<AIAgent>();

        this.isInit = true;
        this.isLocal = true;
    }

    public void InitDummyController()
    {
        //Animator childAnimator = dummy.GetComponent<Animator>();
        //this.animator.avatar = childAnimator.avatar;
        this.dummyControl = GetComponent<DummyAIController>();
        AIAgent tempAgent = GetComponent<AIAgent>();

        this.dummyControl.attackDistance = tempAgent.attackDistance;
        this.dummyControl.damage = tempAgent.GetDamage();

        this.dummyControl.animator = tempAgent.GetComponent<Animator>();
        this.dummyControl.parent = this;
        
		Destroy(tempAgent);
        this.dummyControl.Init(0, ref this.animator);

        this.rigidbody.isKinematic = true;
        //this.animator.enabled = true;

        //Destroy(childAnimator);

        this.isInit = true;
        this.isLocal = false;
    }
    [Command]
    void CmdSendPosition(Vector3 _pos, Quaternion _rot)
    {
        this.RpcReceivePosition(_pos, _rot);
    }

    [ClientRpc]
    void RpcReceivePosition(Vector3 _pos, Quaternion _rot)
    {
        if (this.isLocal || !isInit)
        {
            return;
        }

        int frame = this.sendRate + 1;
        this.dummyControl.lastDirection = (_pos - this.dummyControl.lastDirection) / frame;

        if (this.dummyControl.lastDirection.magnitude > this.movementThreshold)
        {
            this.dummyControl.lastDirection = Vector3.zero;
        }

        Vector3 lastEuler = this.dummyControl.lastRotation.eulerAngles;
        Vector3 newEuler = _rot.eulerAngles;

        if (Quaternion.Angle(this.dummyControl.lastRotation, _rot) < this.angleThreshold)
        {
            this.dummyControl.lastRotation = Quaternion.Euler((newEuler - lastEuler) / frame);
        }
        else
        {
            this.dummyControl.lastRotation = Quaternion.identity;
        }


        this.dummyControl.lastPosition = _pos;
        this.dummyControl.lastRotation = _rot;
    }

    public void ApplyDamage(int _damage)
    {
        this.CmdSendApplyDamage(_damage);
    }

    #region Network Commands

    [Command]
    void CmdSendHealth(int _health)
    {
        this.RpcReceiveHealth(_health);
    }

    [ClientRpc]
    void RpcReceiveHealth(int _health)
    {
        if(this.agent != null)
        {
            this.agent.health = _health;
        }

        if(this.dummyControl != null)
        {
            this.dummyControl.health = _health;
        }
    }

    [Command]
    void CmdSendApplyDamage(int _damage)
    {
        this.RpcReceiveApplyDamage(_damage);
    }

    [ClientRpc]
    void RpcReceiveApplyDamage(int _damage)
    {
        if (!this.isLocal || !isInit)
        {
            return;
        }

        this.agent.ApplyDamage(_damage);
    }

    [Command]
    void CmdSendShooting(bool _shooting)
    {
        this.RpcReceiveShooting(_shooting);
    }

    [ClientRpc]
    void RpcReceiveShooting(bool _shooting)
    {
        if (this.isLocal || !isInit)
        {
            return;
        }

        this.dummyControl.shooting = _shooting;
    }
    [Command]
    void CmdSendDie()
    {
        this.RpcReceiveDie();
    }

    [ClientRpc]
    void RpcReceiveDie()
    {
        Destroy(gameObject);
    }

    [Command]
    void CmdSendTarget(string _tag)
    {
        RpcReceiveTarget(_tag);
    }

    [ClientRpc]
    void RpcReceiveTarget(string _tag)
    {
        if (this.isLocal || !isInit)
        {
            return;
        }

        this.prevTarget = _tag;

        GameObject target = GameObject.FindGameObjectWithTag(_tag);

        if(target != null)
        {
            this.dummyControl.targetObject = target;
            this.dummyControl.targetSyncObject = target.GetComponent<MultiplayerSync>();
        }
    }

    [Command]
    void CmdSendHorizontal(float _horizontal)
    {
        RpcReceiveHorizontal(_horizontal);
    }

    [ClientRpc]
    void RpcReceiveHorizontal(float _horizontal)
    {
        if (this.isLocal || !isInit)
        {
            return;
        }

        this.dummyControl.horizontal = _horizontal;
    }
	#endregion
}
