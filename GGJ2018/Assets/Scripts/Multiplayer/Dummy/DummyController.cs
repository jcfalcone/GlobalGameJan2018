﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyController : Pawn 
{

    public float rotSpeed = 0.1f;
    public float lerpSpeed = 0.1f;
    public float snapDistance = 4;
    public float angleSnap = 40;

    [SerializeField]
    List<GameObject> models;

    [SerializeField]
    List<GameObject> armModel;

    LaserScript laserController;


    public Vector3 lastPosition;
    public Vector3 lastDirection;
    public Quaternion lastRotation;
    public Quaternion lastArmRotation;

    Camera mainCamera;
    bool isVisible;

    public bool shooting;

    public Vector3 shootEndPos;
    public float laserMaxLength;

    int curModel;

	// Use this for initialization
	public void Init (int playerModel, ref Animator _anim) 
	{

        this.models[playerModel].SetActive(true);
        _anim = this.models[playerModel].GetComponent<Animator>();

        this.curModel = playerModel;

        this.laserController = this.models[playerModel].GetComponent<LaserScript>();

		if(_anim != null)
		{
			this.animator = _anim;
			this.animator.cullingMode = AnimatorCullingMode.CullUpdateTransforms;
		}
		
        this.mainCamera = Camera.main;

        this.lastPosition = transform.position;
	}
	
	// Update is called once per frame
	public virtual void Tick () 
	{
        this.Prediction();	

        this.Shoot();

		//TODO: Animation
	}

    bool IsVisibleToCamera()
    {
        if (this.mainCamera == null)
        {
            this.mainCamera = Camera.main;
            return false;
        }

        Vector3 visibleTest = this.mainCamera.WorldToViewportPoint(transform.position + Vector3.up * 1.5f);
        return (visibleTest.x >= 0 && visibleTest.y >= 0) && (visibleTest.x <= 1 && visibleTest.y <= 1) && visibleTest.z >= 0;
    }

    void Prediction()
    {
        Vector3 curPos = transform.position;
        Quaternion curRot = transform.rotation;

        float distance = Vector3.Distance(this.lastPosition, curPos);
        float angle = Vector3.Angle(this.lastRotation.eulerAngles, transform.eulerAngles);

        if (distance > this.snapDistance)
        {
            transform.position = lastPosition;
        }

        if (angle > this.angleSnap)
        {
            transform.rotation = lastRotation;
        }

        curPos += lastDirection;
        curRot *= lastRotation;

        transform.position = Vector3.Lerp(curPos, this.lastPosition, this.lerpSpeed);
        transform.rotation = Quaternion.Slerp(transform.rotation, this.lastRotation, this.rotSpeed);

        if(this.armModel.Count > 0)
        {
            Quaternion curArmRot = this.armModel[this.curModel].transform.rotation;
            
            float angleArm = Vector3.Angle(this.lastArmRotation.eulerAngles, this.armModel[this.curModel].transform.localEulerAngles);

            if (angleArm > this.angleSnap)
            {
                this.armModel[this.curModel].transform.localRotation = this.lastArmRotation;
            }

            curArmRot *= this.lastArmRotation;
            this.armModel[this.curModel].transform.localRotation = Quaternion.Slerp(this.armModel[this.curModel].transform.localRotation, this.lastArmRotation, this.rotSpeed);
        }
    }

    protected virtual void Shoot()
    {
        if(this.shooting)
        {
            this.laserController.FireLaser(this.shootEndPos);

            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit raycastHit;

            if (Physics.Raycast(ray, out raycastHit, laserMaxLength))
            {
                if (raycastHit.collider.gameObject.CompareTag("Enemy"))
                {
                    Pawn unitPawn = raycastHit.collider.GetComponent<Pawn>();

                    if(unitPawn)
                    {
                        unitPawn.ApplyDamage(this.Damage);
                    }
                }
            }
        }
        else
        {
            this.laserController.StopLaser();
        }
    }
}
