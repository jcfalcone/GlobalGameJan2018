﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyAIController : DummyController {

	[SerializeField]
	List<ParticleSystem> particles;
	public GameObject targetObject;

	public MultiplayerSync targetSyncObject;

	public float horizontal;
	public float attackDistance;

	public int damage;

	int currGun = 0;

	public MultiplayerAISync parent;

	public override void Tick()
	{
		base.Tick();

		this.animator.SetFloat("Horizontal", this.horizontal);
	}

	protected override void Shoot()
	{
		this.animator.Play("Attack_Animation");
	}

	public void FireGun(int _currGun)
	{
		if(FireCastTarget())
		{
			if(targetSyncObject)
			{
				targetSyncObject.ApplyDamage(damage);
			}
			else
			{
				targetObject.GetComponentInChildren<Pawn>().ApplyDamage(damage);
			}
		}

		this.particles[this.currGun].Play();

		this.currGun = _currGun;
	}

	bool FireCastTarget()
	{
		float offset = 1.5f;
		float distance = attackDistance;

		Vector3 origin = transform.position;
		origin.y += offset;

		Vector3 direction = transform.forward;

		Debug.DrawRay(origin, direction * distance, Color.red);
		RaycastHit hit;
		if(Physics.Raycast(origin, direction, out hit, distance))
		{
			if (hit.transform.CompareTag("Player1") || hit.transform.CompareTag("Player2") || hit.transform.CompareTag("Core"))
				return true;
		}

		return false;
	}

	public override void ApplyDamage(int _damage)
	{
		this.parent.ApplyDamage(_damage);
	}
}
