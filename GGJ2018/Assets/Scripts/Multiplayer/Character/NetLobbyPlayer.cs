﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class NetLobbyPlayer : NetworkLobbyPlayer 
{
	[SyncVar(hook = "CmdSendLevel")]
	public string targetLevelName;

	[SyncVar]
	public string charName = "Unknown";

	[SyncVar]
	public string model = "";

	public override void OnClientEnterLobby()
	{
		base.OnClientEnterLobby();

		if (this.isServer)
		{
			NetworkMatchData data = NetworkMaster.instance.currMatch;
			this.targetLevelName = NetworkMatchData.StrToMap(data.map.ToString());
			this.CmdSendLevel(this.targetLevelName);
		}

		if (string.IsNullOrEmpty(this.model))
		{
			//if model is null, that means it's the local player and it hasn't init yet
			return;
		}

		NetworkLobby.instance.AddPlayerLobby(this);
		//StartCoroutine("DelayBroadcastName");
	}

	public override void OnStartLocalPlayer()
	{
		base.OnStartLocalPlayer();
		this.SetupPlayer();
	}

	void SetupPlayer()
	{
		if (!isServer)
		{
			this.SendReadyToBeginMessage();
		}

		this.charName = NetworkMaster.instance.playerPrefix + "_" + this.netId;
		this.model = "NudeWoman";

		this.CmdBroadcastPlayer(this.netId.Value, this.charName);

		NetworkLobby.instance.AddPlayerLobby(this);
	}

	[Command]
	public void CmdBroadcastPlayer(uint _playerid, string _name)
	{
		RpcBroadcastPlayer(_playerid, _name);
	}

	[ClientRpc]
	public void RpcBroadcastPlayer(uint _playerid, string _playerName)
	{
		if (isLocalPlayer)
		{
			return;
		}

		LobbyPlayerInfo info = NetworkLobby.instance.GetPlayer(_playerid);

		if (info != null)
		{
			info.lobbyPlayer.charName = _playerName;
		}

		this.charName = _playerName;

		NetworkLobby.instance.AddPlayerLobby(this);
	}

	#region Network Commands
	[Command]
	void CmdSendLevel(string _level)
	{
		this.RpcReceiveLevel(_level);
	}

	[ClientRpc]
	void RpcReceiveLevel(string _level)
	{
		this.targetLevelName = _level;
	}
	#endregion
}