﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class MultiplayerSync : NetworkBehaviour 
{
    [SyncVar]
    public string levelName;

	[SerializeField]
	GameObject playerDummy;

	[SerializeField]
	GameObject playerLocal;

	[SerializeField]
	Rigidbody rigidbody;

    public int sendRate = 4;
    public float movementThreshold = 0.1f;
    public float angleThreshold = 5; 

    public bool isInit;
    bool isLocal;
    bool levelLoaded;

	int countRate = 0;

	Animator animator;

    DummyController dummyControl;
	FirstPersonController controller;
    LaserScript weaponController;
    Character charController;

	Vector3 spawnPoint;

    Camera mainCamera;

    #region SyncVars
    bool prevShooting;

    int prevHealth;

    Vector3 prevFinalShootPos;
    #endregion

    string tagObj;

    int currIndex = 0;

	// Use this for initialization
	public void Start () 
	{
        this.levelLoaded = false;

        if (this.isServer)
        {
            NetworkMatchData matchData = NetworkMaster.instance.currMatch;
            //this.levelName = NetworkMatchData.StrToMap(matchData.map.ToString());
            this.CmdSendLevel(this.levelName);
        }

        if (this.isLocalPlayer)
        {
            this.Initialize();
        }
        else
        {
            this.InitDummyController();
        }
	}

    public void Init(int _currIndex)
    {
        this.currIndex = _currIndex;

        this.tagObj = "Player"+(this.GetPlayerId() + 1).ToString();
    }
	
	// Update is called once per frame
	void Update () 
	{
        if (!this.isInit)
        {
            return;
        }

        if (!this.isLocalPlayer)
        {
            this.DummyTick();
        }

        if (this.countRate < this.sendRate)
        {
            this.countRate++;
            return;
        }

        this.countRate = 0;

        if (this.isLocalPlayer)
        {
            this.LocalPlayerTick();
        }
	}

    void LateUpdate()
    {
        if (!this.isInit)
        {
            return;
        }

        if (this.countRate != sendRate)
        {
            return;
        }

        if (this.isLocalPlayer)
        {
            this.LocalPlayerLateTick();
        }
    }

	void LocalPlayerTick()
	{

        if(this.charController.laseController != this.weaponController)
        {
            this.weaponController = this.charController.laseController;
            this.controller = this.charController.firstPersonController;
            this.mainCamera = this.charController.mainCamera;
        }

        if(!this.controller)
        {
            return;
        }

        if(this.prevShooting != this.weaponController.shooting)
        {
            this.CmdSendShooting(this.weaponController.shooting);
        }

        if(this.weaponController.shooting)
        {
            this.CmdSendEndShootPos(this.weaponController.endPosition);
        }

        if(this.prevHealth != this.charController.health)
        {
            this.CmdSendHealth(this.charController.health);
        }
        
        if(this.isServer)
        {
            if(!string.IsNullOrEmpty(this.tagObj))
            {
                this.CmdSendTag(this.tagObj);
            }
        }

        this.prevHealth = this.charController.health;
        this.prevShooting = this.weaponController.shooting;
	}

	void LocalPlayerLateTick()
    {
        this.CmdSendPosition(this.controller.transform.position, this.controller.transform.rotation);
        this.CmdSendCamRotation(this.mainCamera.transform.localRotation);
	}

    void DummyTick()
    {
        if(this.isServer)
        {
            if(!string.IsNullOrEmpty(this.tagObj))
            {
                Debug.Log(this.tagObj, gameObject);
                this.CmdSendTag(this.tagObj);
            }
        }

        this.dummyControl.Tick();
    }
	
    void Initialize()
    {
        if(string.IsNullOrEmpty(this.levelName))
        {
            Debug.LogError("MultiplayerLevelLoader: Level is null or Empty", gameObject);
            this.levelName = "Level1";
            //return;
        }

        //Debug.Log("Loading Level: "+this.levelName);

        StartCoroutine(this.LoadLevel());
    }

    IEnumerator LoadLevel()
    {
        yield return SceneManager.LoadSceneAsync(this.levelName, LoadSceneMode.Additive);

        //SceneManager.SetActiveScene(2);s
        this.InitPlayerController();

        this.levelLoaded = true;
        Debug.Log("Level Loaded");
    }

	public void InitPlayerController()
    {
        this.isInit = true;
        this.isLocal = false;

        
        Destroy(GetComponent<Rigidbody>());
        Destroy(GetComponent<CapsuleCollider>());

        this.spawnPoint = LevelMaster.instance.GetSpawnPosition(this.GetPlayerId());

        GameObject player = Instantiate(this.playerLocal, transform);
        player.name = "LocalPlayer";
        player.transform.localPosition = Vector3.zero;
        player.gameObject.tag = this.tagObj;
        player.transform.parent.position = this.spawnPoint;

        this.mainCamera = Camera.main;

        Destroy(this.animator);

        this.controller = player.GetComponent<FirstPersonController>();
        this.charController = player.GetComponent<Character>();
        this.weaponController = player.GetComponentInChildren<LaserScript>();

        this.charController.spawnPosition = this.spawnPoint;
        this.charController.health = 100;

        if(this.isServer && !LevelMaster.instance.startWave)
        {
            StartCoroutine(LevelMaster.instance.StartAI());
        }
    }

    public void InitDummyController()
    {
        this.isInit = true;
        this.isLocal = true;
        //this.animator.enabled = true;

        if(LevelMaster.instance)
        {
            this.spawnPoint = LevelMaster.instance.GetSpawnPosition(this.GetPlayerId());
        }

        GameObject dummy = Instantiate(this.playerDummy, this.transform);
        dummy.name = "DummyPlayer";
        dummy.transform.parent = transform;
        dummy.transform.localPosition = Vector3.zero;

        if(!string.IsNullOrEmpty(this.tagObj))
        {
            dummy.gameObject.tag = this.tagObj;
        }

        if(this.isServer)
        {
            this.CmdSendSpawnPoint(this.spawnPoint);
        }

        //Animator childAnimator = dummy.GetComponent<Animator>();

        //this.animator.avatar = childAnimator.avatar;

        this.dummyControl = dummy.GetComponent<DummyController>();
        this.dummyControl.Init(this.GetPlayerId(), ref this.animator);

        this.rigidbody.isKinematic = true;

        if (isServer)
        {
            this.CmdSendTag(this.tagObj);
        }

        //Destroy(childAnimator);
    }

    public void ApplyDamage(int _damage)
    {
        this.CmdSendApplyDamage(_damage);
    }

    #region GetSet
    public int GetPlayerId()
    {
        return currIndex;
    }
    #endregion

    #region Network Commands

    [Command]
    void CmdSendHealth(int _health)
    {
        this.RpcReceiveHealth(_health);
    }

    [ClientRpc]
    void RpcReceiveHealth(int _health)
    {
        if(this.charController != null)
        {
            this.charController.health = _health;
        }

        if(this.dummyControl != null)
        {
            this.dummyControl.health = _health;
        }
    }

    [Command]
    void CmdSendPosition(Vector3 _pos, Quaternion _rot)
    {
        this.RpcReceivePosition(_pos, _rot);
    }

    [ClientRpc]
    void RpcReceivePosition(Vector3 _pos, Quaternion _rot)
    {
        if (!this.isLocal || !isInit)
        {
            return;
        }

        int frame = this.sendRate + 1;
        this.dummyControl.lastDirection = (_pos - this.dummyControl.lastDirection) / frame;

        if (this.dummyControl.lastDirection.magnitude > this.movementThreshold)
        {
            this.dummyControl.lastDirection = Vector3.zero;
        }

        Vector3 lastEuler = this.dummyControl.lastRotation.eulerAngles;
        Vector3 newEuler = _rot.eulerAngles;

        if (Quaternion.Angle(this.dummyControl.lastRotation, _rot) < this.angleThreshold)
        {
            this.dummyControl.lastRotation = Quaternion.Euler((newEuler - lastEuler) / frame);
        }
        else
        {
            this.dummyControl.lastRotation = Quaternion.identity;
        }


        this.dummyControl.lastPosition = _pos;
        this.dummyControl.lastRotation = _rot;
    }

    [Command]
    void CmdSendLevel(string _level)
    {
        this.RpcReceiveLevel(_level);
    }

    [ClientRpc]
    void RpcReceiveLevel(string _level)
    {
        this.levelName = _level;
    }

    [Command]
    void CmdSendShooting(bool _shooting)
    {
        this.RpcReceiveShooting(_shooting);
    }

    [ClientRpc]
    void RpcReceiveShooting(bool _shooting)
    {
        if (this.isLocalPlayer || !this.isInit)
        {
            return;
        }

        this.dummyControl.shooting = _shooting;
    }
    [Command]
    void CmdSendCamRotation(Quaternion _rot)
    {
        this.RpcReceiveCamRotation(_rot);
    }

    [ClientRpc]
    void RpcReceiveCamRotation(Quaternion _rot)
    {
        if (this.isLocalPlayer || !isInit)
        {
            return;
        }
        
        int frame = this.sendRate + 1;

        Vector3 lastEuler = this.dummyControl.lastArmRotation.eulerAngles;
        Vector3 newEuler = _rot.eulerAngles;

        if (Quaternion.Angle(this.dummyControl.lastArmRotation, _rot) < this.angleThreshold)
        {
            this.dummyControl.lastArmRotation = Quaternion.Euler((newEuler - lastEuler) / frame);
        }
        else
        {
            this.dummyControl.lastArmRotation = Quaternion.identity;
        }

        this.dummyControl.lastArmRotation = _rot;
    }

    [Command]
    void CmdSendEndShootPos(Vector3 _pos)
    {
        this.RpcReceiveEndShootPos(_pos);
    }

    [ClientRpc]
    void RpcReceiveEndShootPos(Vector3 _pos)
    {
        if (this.isLocalPlayer || !isInit)
        {
            return;
        }

        this.dummyControl.shootEndPos = _pos;
    }

    [Command]
    void CmdSendApplyDamage(int _damage)
    {
        this.RpcReceiveApplyDamage(_damage);
    }

    [ClientRpc]
    void RpcReceiveApplyDamage(int _damage)
    {
        if (!this.isLocalPlayer || !isInit)
        {
            return;
        }

        this.charController.ApplyDamage(_damage);
    }

    [Command]
    void CmdSendTag(string _tag)
    {
        this.RpcReceiveTag(_tag);
    }

    [ClientRpc]
    void RpcReceiveTag(string _tag)
    {
        this.tagObj = _tag;

        if(this.dummyControl)
        {
            this.dummyControl.gameObject.tag = _tag;
        }

        if(this.charController)
        {
            this.charController.gameObject.tag = _tag;
        }
    }

    [Command]
    void CmdSendSpawnPoint(Vector3 _pos)
    {
        this.RpcReceiveSpawnPoint(_pos);
    }

    [ClientRpc]
    void RpcReceiveSpawnPoint(Vector3 _pos)
    {
        if(this.charController)
        {
            this.charController.spawnPosition = _pos;
            this.charController.transform.parent.position = _pos;
        }
    }
	#endregion
}