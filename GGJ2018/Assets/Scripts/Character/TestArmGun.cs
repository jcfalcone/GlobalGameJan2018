﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestArmGun : Weapon {

    private RaycastHit hit;

    private float m_castDistance;

    public GameObject m_objectHit;

    public Color c1 = Color.blue;
    public Color c2 = Color.yellow;
    public int m_lengthOfRenderer = 100;

    [SerializeField]
    private LineRenderer lineRenderer; 

	// Use this for initialization
	void Start () {
        m_castDistance = 200.0f;
       
	}
	
	// Update is called once per frame
	void Update () {
		
        if (Physics.Raycast (transform.position, transform.forward , out hit, m_castDistance)) {
                m_objectHit = hit.collider.gameObject;
            Debug.Log (m_objectHit.name);
            } else {
                m_objectHit = null;
            }


        lineRenderer.SetPosition (1, (this.transform.position));
       
	}
}
   