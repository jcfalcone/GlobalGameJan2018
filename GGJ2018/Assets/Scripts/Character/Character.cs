﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Character : Pawn 
{
    public UICoolDowns uICoolDown;

    public List<GameObject> playerModel = new List<GameObject>();
    public List<LaserScript> listLaser = new List<LaserScript>();

    [SerializeField]
    Camera deathCam;

    [SerializeField]
    private KeyCode m_tapForward;
    [SerializeField]
    private KeyCode m_tapBackwards;
    [SerializeField]
    private KeyCode tapLeft;
    [SerializeField]
    private KeyCode tapRight;

    private int m_forwardTotal = 0;
    [SerializeField]
    private float m_forwardTimeDelay = 0;
    private int m_backwardTotal = 0;
    [SerializeField]
    private float m_backwardTimeDelay = 0;
    private int m_leftTotal = 0;
    [SerializeField]
    private float m_leftTimeDelay = 0;
    private int m_rightTotal = 0;
    [SerializeField]
    private float m_rightTimeDelay = 0;

    private float m_player1Health;

    private float m_player2Health;

    private float m_dashDuration = 0;
    [SerializeField]
    private float m_dashTimeLength;

    [SerializeField]
    private float m_coolDown = 0;
    [SerializeField]
    private float m_coolDownTime;

    private bool m_usedDash;
    private bool m_leaveEnergy;

    public float xVel = 0;
    public float yVel = 0;

    [HideInInspector]
    public Vector3 spawnPosition;

    public CharacterController m_controller;

    Pawn playerOtherHealth;

    public Camera mainCamera;

    public FirstPersonController firstPersonController;

    public LaserScript laseController;


    float deathTime;
	
    void Start () 
    {
        uICoolDown.HandleBarEnergy(1);
        m_leaveEnergy = true;

        this.mainCamera = Camera.main;
        this.firstPersonController = GetComponent<FirstPersonController>();
        this.playerOtherHealth = null;
       
    }

	// Update is called once per frame
	void Update () 
    {
        if(this.CompareTag("Player2"))
        {
            if(this.playerOtherHealth == null)
            {
                GameObject otherPlayer = GameObject.FindGameObjectWithTag("Player1");

                if(otherPlayer)
                {
                    this.playerOtherHealth = otherPlayer.GetComponent<Pawn>();
                }
            }
            else
            {
                if (m_player1Health != this.playerOtherHealth.health)
                {
                    uICoolDown.HandleBarHealth(m_player1Health);
                    m_player1Health = Mathf.Lerp(m_player1Health, this.playerOtherHealth.health, Time.deltaTime);
                }
                
                if (m_player2Health != this.health)
                {
                    uICoolDown.HandleBarHealthTwo(m_player2Health);
                    m_player2Health = Mathf.Lerp(m_player2Health, this.health, Time.deltaTime);
                }
            }

            if(!this.playerModel[1].activeSelf && this.isAlive)
            {
                this.playerModel[0].SetActive(false);
                this.playerModel[1].SetActive(true);

                this.firstPersonController.Start();
            }

            this.uICoolDown = this.playerModel[1].GetComponentInChildren<UICoolDowns>();
            this.laseController = this.listLaser[1];
            this.laseController.uICoolDown = this.uICoolDown;
        }
        else if(this.CompareTag("Player1"))
        {
            if(this.playerOtherHealth == null)
            {
                GameObject otherPlayer = GameObject.FindGameObjectWithTag("Player2");

                if(otherPlayer)
                {
                    this.playerOtherHealth = otherPlayer.GetComponent<Pawn>();
                }
            }
            else
            {
                if (m_player1Health != this.playerOtherHealth.health)
                {
                    uICoolDown.HandleBarHealth(m_player1Health);
                    m_player1Health = Mathf.Lerp(m_player1Health, this.health, Time.deltaTime);
                }
                
                if (m_player2Health != this.health)
                {
                    uICoolDown.HandleBarHealthTwo(m_player2Health);
                    m_player2Health = Mathf.Lerp(m_player2Health, this.playerOtherHealth.health, Time.deltaTime);
                }
            }

            if(!this.playerModel[0].activeSelf && this.isAlive)
            {
                this.playerModel[0].SetActive(true);
                this.playerModel[1].SetActive(false);

                this.firstPersonController.Start();
            }

            this.uICoolDown = this.playerModel[0].GetComponentInChildren<UICoolDowns>();
            this.laseController = this.listLaser[0];
            this.laseController.uICoolDown = this.uICoolDown;
        }

        if(!this.isAlive)
        {
            if(this.firstPersonController.enabled)
            {
                this.firstPersonController.enabled = false;
            }

            if(this.laseController.enabled)
            {
                this.laseController.enabled = false;
            }

            this.playerModel[0].SetActive(false);
            this.playerModel[1].SetActive(false);

            if(!this.deathCam.gameObject.activeSelf)
            {
                this.deathCam.gameObject.SetActive(true);
                this.deathCam.transform.rotation = this.mainCamera.transform.rotation;
            }

            if(this.playerOtherHealth)
            {
                //Vector3 direction = this.deathCam.transform.position - this.playerOtherHealth.transform.position;
                Quaternion lookRot = Quaternion.LookRotation(this.playerOtherHealth.transform.position);
                this.deathCam.transform.rotation = Quaternion.Slerp(this.deathCam.transform.rotation, lookRot, Time.deltaTime);
            }

            this.deathTime += Time.deltaTime;

            if(this.deathTime > 10f)
            {
                transform.position = this.spawnPosition;
                this.health = 100;
                this.laseController.enabled = true;
                this.firstPersonController.enabled = true;
            }
            
            return;
        }

        if(this.deathCam.gameObject.activeSelf)
        {
            this.deathCam.gameObject.SetActive(false);
            this.deathTime = 0;
        }

        if (!m_leaveEnergy)
        {
            uICoolDown.HandleBarEnergy(m_coolDown);
        }

        if (m_usedDash)
        {
            m_coolDown += Time.deltaTime;
            m_leaveEnergy = false;
            if (m_coolDown > m_coolDownTime)
            {
                Debug.Log("Turning cooldown off");
                m_usedDash = false;
                m_coolDown = 0;
                m_leaveEnergy = true;
                uICoolDown.HandleBarEnergy(1);
            }
            else
            {
                m_forwardTotal = 0;
                m_backwardTotal = 0;
                m_leftTotal = 0;
                m_rightTotal = 0;
            }
        }      
           
        //dash forward
        if (Input.GetKey(m_tapForward))
        {
            m_controller.Move(transform.forward * yVel);
        }

        if (Input.GetKeyDown(m_tapForward))
        {
            m_forwardTotal += 1;
        }
        if (Input.GetKeyUp(m_tapForward))
        {
            xVel = 0;
            m_controller.Move (transform.forward * 0);
        }
        if ((m_forwardTotal == 1) && (m_forwardTimeDelay < 0.5f) && !m_usedDash)
        {
            m_forwardTimeDelay += Time.deltaTime;
        }
        if ((m_forwardTotal == 1) && (m_forwardTimeDelay >= 0.5f))
        {
            m_forwardTimeDelay = 0;
            m_forwardTotal = 0;
        }
        if ((m_forwardTotal == 2) && (m_forwardTimeDelay < 0.5f) && !m_usedDash)
        {
            SoundManager.instance.Play(SoundManager.AudioClips.Dashing);
            Debug.Log("dashing forward");
            yVel = 0.2f;
            m_forwardTotal = 0;
            m_usedDash = true;
            m_forwardTimeDelay = 0;
        }
        if ((m_forwardTotal == 2) && (m_forwardTimeDelay >= 0.5f))
        {
            yVel = 0;
            m_forwardTotal = 0;
            m_forwardTimeDelay = 0;
        }
        if (yVel == 0.2f)
        {
            m_dashDuration += Time.deltaTime;
        }
        if (m_dashDuration > m_dashTimeLength)
        {
            yVel = 0;
            m_dashDuration = 0;
            m_forwardTotal = 0;
            m_forwardTimeDelay = 0;
        }
           
        //dash back
        if (Input.GetKey(m_tapBackwards))
        {
            m_controller.Move(transform.forward * -yVel);
        }

        if (Input.GetKeyDown(m_tapBackwards))
        {
            m_backwardTotal += 1;
        }
        if (Input.GetKeyUp(m_tapBackwards))
        {
            yVel = 0;
            m_controller.Move (transform.forward * 0);
        }
        if ((m_backwardTotal == 1) && (m_backwardTimeDelay < 0.5f) && !m_usedDash)
        {
            m_backwardTimeDelay += Time.deltaTime;
        }
        if ((m_backwardTotal == 1) && (m_backwardTimeDelay >= 0.5f))
        {
            m_backwardTimeDelay = 0;
            m_backwardTotal = 0;
        }
        if (!m_usedDash)
        {
            if ((m_backwardTotal == 2) && (m_backwardTimeDelay < 0.5f))
            {
                SoundManager.instance.Play(SoundManager.AudioClips.Dashing);
                Debug.Log("dashing backwards");
                yVel = 0.2f;
                m_backwardTotal = 0;
                m_usedDash = true;
                m_backwardTimeDelay = 0;
            }
        }
        if ((m_backwardTotal == 2) && (m_backwardTimeDelay >= 0.5f))
        {
            yVel = 0;
            m_backwardTotal = 0;
            m_backwardTimeDelay = 0;
        }
        if (yVel == 0.2f)
        {
            m_dashDuration += Time.deltaTime;
        }
        if (m_dashDuration > m_dashTimeLength)
        {
            yVel = 0;
            m_dashDuration = 0;
            m_backwardTotal = 0;
            m_backwardTimeDelay = 0;
        }
       

        //dash right
        if (Input.GetKey(tapRight))
        {
            m_controller.Move(transform.right * xVel);
        }
     
        if (Input.GetKeyDown(tapRight))
        {
            m_rightTotal += 1;
        }
        if (Input.GetKeyUp(tapRight))
        {
            xVel = 0;
            m_controller.Move (transform.right * 0);
        }
        if ((m_rightTotal == 1) && (m_rightTimeDelay < 0.5f) && !m_usedDash)
        {
            m_rightTimeDelay += Time.deltaTime;
        }
        if ((m_rightTotal == 1) && (m_rightTimeDelay >= 0.5f))
        {
            m_rightTimeDelay = 0;
            m_rightTotal = 0;
        }
        if ((m_rightTotal == 2) && (m_rightTimeDelay < 0.5f) && !m_usedDash)
        {
            SoundManager.instance.Play(SoundManager.AudioClips.Dashing);
            Debug.Log("dashing");
            xVel = 0.25f;
            m_rightTotal = 0;
            m_usedDash = true;
            m_rightTimeDelay = 0;
        }
        if ((m_rightTotal == 2) && (m_rightTimeDelay >= 0.5f))
        {
            xVel = 0;
            m_rightTotal = 0;
            m_rightTimeDelay = 0;
        }
        if (xVel == 0.25f)
        {
            m_dashDuration += Time.deltaTime;
        }
        if (m_dashDuration > m_dashTimeLength)
        {
            xVel = 0;
            m_dashDuration = 0;
            m_rightTotal = 0;
            m_rightTimeDelay = 0;
        }

        //dash left 
        if (Input.GetKey(tapLeft))
        {
            m_controller.Move(transform.right * -xVel);
        }

        if (Input.GetKeyDown(tapLeft))
        {
            m_leftTotal += 1;
        }
        if (Input.GetKeyUp(tapLeft))
        {
            xVel = 0;
            m_controller.Move (transform.right * 0);
        }
        if ((m_leftTotal == 1) && (m_leftTimeDelay < 0.5f) && !m_usedDash)
        {
            m_leftTimeDelay += Time.deltaTime;
        }
        if ((m_leftTotal == 1) && (m_leftTimeDelay >= 0.5f))
        {
            m_leftTimeDelay = 0;
            m_leftTotal = 0;
        }
        if ((m_leftTotal == 2) && (m_leftTimeDelay < 0.5f) && !m_usedDash)
        {
            SoundManager.instance.Play(SoundManager.AudioClips.Dashing);
            Debug.Log("dashing");
            xVel = 0.25f;
            m_leftTotal = 0;
            m_usedDash = true;
            m_leftTimeDelay = 0;
        }
        if ((m_leftTotal == 2) && (m_leftTimeDelay >= 0.5f))
        {
            xVel = 0;
            m_leftTotal = 0;
            m_leftTimeDelay = 0;
        }
        if (xVel == 0.25f)
        {
            m_dashDuration += Time.deltaTime;
        }
        if (m_dashDuration > m_dashTimeLength)
        {
            xVel = 0;
            m_dashDuration = 0;
            m_leftTotal = 0;
            m_leftTimeDelay = 0;
        }

	}

    public override void Die()
    {
    }

}
