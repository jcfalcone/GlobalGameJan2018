﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Weapon : MonoBehaviour {

	//					WEAPON RELATED STATS
	 
	public enum WeaponType 
	{
		invalid = -1,
		armweapon
	}

	[Header("WeaponStatsNStuff")]
	[SerializeField]
	protected WeaponType m_weaponType;

	//reset after each bullet is fired
	protected float m_nextAmmoDown;
    protected float m_nextIncrease;
    protected float m_overheatStatus;
    protected float m_overheatTUp;
    protected bool m_overheated;


    [SerializeField]
    protected float m_overheatRate;
    [SerializeField]
    protected float m_maxHeatStatus;
    [SerializeField]
    [Range(0f, 1f)]
    protected float m_overheatTIncreaseRate;
    [SerializeField]
    protected float m_sizeFireRate;
    [SerializeField]
    protected float m_ammoFireRate;
   
	[SerializeField]
	protected GameObject m_gun;

    [SerializeField]
    protected Transform[] m_jerkLocations;

	//	amount of jerk back the gun gives after firing
	[SerializeField]
	protected float m_jerk;
}
