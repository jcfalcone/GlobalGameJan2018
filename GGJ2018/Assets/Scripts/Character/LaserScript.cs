﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : Weapon {

    public int Damage;

    public UICoolDowns uICoolDown;

    [SerializeField]
    private LineRenderer laserLine;
  
    public ParticleSystem m_matrix;

    [SerializeField]
    private float laserWidth;

    [SerializeField]
    private float laserMaxLength;

    Vector3 targetPosition;

    public Vector3 endPosition;

    public bool shooting; 
    public bool useUpdate = true;

    private bool m_pulseDone;

    public Transform m_startPosition;

    private float m_startJerkTime;

    private float jerkJourneyLength;

    public GameObject m_HUD;

	// Use this for initialization
	void Start () 
    {
        if(m_matrix)
        {
            m_matrix.Stop();
            m_matrix.Clear();
        }

        m_pulseDone = false;

        Vector3[] initLaserPositions = new Vector3[2]
        {
            Vector3.zero,
            Vector3.zero
        };
        laserLine.SetPositions(initLaserPositions);	
        laserLine.widthMultiplier = laserWidth;

    }
	
	// Update is called once per frame
	void Update () 
    {
        if(this.uICoolDown)
        {
            this.uICoolDown.HandleBarHeat(m_overheatStatus);
        }
        
        if (!shooting)
        {
            if (Time.time > m_overheatTUp && m_overheatStatus > 0)
            {
                Debug.Log("decreasing");
                m_overheatTUp = Time.time + m_overheatTIncreaseRate;
                m_overheatStatus -= 15f / 1000f;
                if (m_overheatStatus < 0)
                {
                    m_overheatStatus = 0;
                }
            }
            if (m_overheatStatus <= 0)
            {
                m_overheated = false;
            }
        }
        if (laserLine.endWidth >= 0.25f)
        {
            Debug.Log("Ending fire");
            m_pulseDone = true;
            ResetLaserSize();
        }
        else
        {
            m_pulseDone = false;
        }
        
        
        if(this.useUpdate)
        {
            switch (m_pulseDone) {

                case false:

                    if (Input.GetKeyDown(KeyCode.Mouse0) && SoundManager.instance)
                    {
                        SoundManager.instance.Play(SoundManager.AudioClips.PlayerShot);
                    }
                    if (Input.GetKey(KeyCode.Mouse0))
                    { 
                        if (m_matrix && !m_matrix.isPlaying)
                        {
                            m_matrix.Play();
                        }
                        FireLaser();
                        shooting = true;
                        m_startJerkTime = Time.time;
                        jerkJourneyLength = Vector3.Distance(m_jerkLocations[1].position, m_jerkLocations[0].position);
                        float distCovered = (Time.time - m_startJerkTime) * 1;
                        float fracJourney = distCovered / jerkJourneyLength;
                        m_gun.transform.position = Vector3.Lerp(m_jerkLocations[1].position, m_jerkLocations[0].localPosition, fracJourney);
                    } 
                    else if (Input.GetKeyUp(KeyCode.Mouse0))
                    {
                        if(m_matrix)
                        {
                            m_matrix.Stop();
                        }
                        
                        shooting = false;
                        ResetLaserSize();
                        this.StopLaser();
                        m_startJerkTime = Time.time;
                        jerkJourneyLength = Vector3.Distance(m_jerkLocations[0].position, m_jerkLocations[1].position);
                        float distCovered = (Time.time - m_startJerkTime) * 1;
                        float fracJourney = distCovered / jerkJourneyLength;
                        m_gun.transform.position = Vector3.Lerp(m_jerkLocations[0].position, m_jerkLocations[1].localPosition, fracJourney);  
                    } 
                    break;
            }
        }

    }
    public void FireLaser () 
    {
            if (m_startPosition == null)
            {
                targetPosition = transform.position; 
            }
            else
            {
                targetPosition = m_startPosition.position;
            }

            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit raycastHit;
            endPosition = targetPosition + (laserMaxLength * transform.forward);


            if (Physics.Raycast(ray, out raycastHit, laserMaxLength))
            {
                endPosition = raycastHit.point;
                if (raycastHit.collider.gameObject.CompareTag("Enemy") && Time.time > m_nextIncrease && !m_overheated)
                {
                    m_nextIncrease = Time.time + m_sizeFireRate;
                    EnlargeLaser();
                    if (Time.time > m_overheatTUp)
                    {
                        
                        m_overheatTUp = Time.time + m_overheatTIncreaseRate;
                        if (m_overheatStatus < 1)
                        {
                            m_overheatStatus += 10f / 1000f;
                            if (m_overheatStatus > 1)
                            {
                                m_overheatStatus = 1;
                            }
                        }
                        else if (m_overheatStatus >= 1)
                        {
                            m_overheated = true;
                            OverHeat();
                        }
                    }

                    if (Time.time > m_nextAmmoDown)
                    {
                        if(raycastHit.collider != null)
                        {
                            Pawn unitPawn = raycastHit.collider.GetComponent<Pawn>();

                            if(unitPawn)
                            {
                                unitPawn.ApplyDamage(this.Damage);
                            }
                        }
                        m_nextAmmoDown = Time.time + m_ammoFireRate;
                    }
                }
                else if (raycastHit.collider.gameObject.CompareTag("Switch"))
                {
                    raycastHit.collider.gameObject.GetComponent<SwitchShoot>().WasTriggered(true);
                }
                else if (!raycastHit.collider.gameObject.CompareTag("Enemy"))
                {
                   ResetLaserSize();  
                }

                if (!m_overheated)
                {
                    laserLine.enabled = true;
                }

            }
            
        laserLine.SetPosition(0, targetPosition);
        laserLine.SetPosition(1, endPosition);
    }
    public void FireLaser (Vector3 _endPos) 
    {
        laserLine.enabled = true;

        if (m_startPosition == null)
        {
            targetPosition = transform.position; 
        }
        else
        {
            targetPosition = m_startPosition.position;
        }

        endPosition = _endPos;
            
        laserLine.SetPosition(0, targetPosition);
        laserLine.SetPosition(1, endPosition);
    }
    public void StopLaser () 
    {
        laserLine.enabled = false;
    }

    private void EnlargeLaser () {
        laserLine.startWidth += 0.005f;
        laserLine.endWidth += 0.005f;
    }

    public void ResetLaserSize () {
        laserLine.startWidth = 0.1f;
        laserLine.endWidth = 0.1f;
    }

    public void OverHeat () {
        shooting = false;
        ResetLaserSize();
        this.StopLaser();
        m_startJerkTime = Time.time;
        jerkJourneyLength = Vector3.Distance(m_jerkLocations[0].position, m_jerkLocations[1].position);
        float distCovered = (Time.time - m_startJerkTime) * 1;
        float fracJourney = distCovered / jerkJourneyLength;
        m_gun.transform.position = Vector3.Lerp(m_jerkLocations[0].position, m_jerkLocations[1].localPosition, fracJourney);  
    }
      
}